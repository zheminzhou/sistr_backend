"""added bongori subspecies enum option

Revision ID: 360aeb3affac
Revises: 1859e4940868
Create Date: 2015-02-25 03:31:09.566859

"""

# revision identifiers, used by Alembic.
revision = '360aeb3affac'
down_revision = '1859e4940868'

from alembic import op


def upgrade():
    op.execute("ALTER table genome rename COLUMN subspecies to _old_subspecies;")
    op.execute("ALTER TYPE ck_salmonella_subspecies RENAME TO _old_ck_salmonella_subspecies;")
    op.execute(
        "CREATE TYPE ck_salmonella_subspecies AS ENUM ('arizonae', 'indica', 'enterica', 'houtenae', 'diarizonae', 'bongori', 'salamae');")
    op.execute("ALTER TABLE genome ADD COLUMN subspecies ck_salmonella_subspecies;")
    op.execute("UPDATE genome SET subspecies = _old_subspecies::text::ck_salmonella_subspecies;")
    op.execute("ALTER TABLE genome DROP COLUMN _old_subspecies;")
    op.execute("DROP TYPE _old_ck_salmonella_subspecies;")


def downgrade():
    op.execute("ALTER table genome rename COLUMN subspecies to _old_subspecies;")
    op.execute("ALTER TYPE ck_salmonella_subspecies RENAME TO _old_ck_salmonella_subspecies;")
    op.execute(
        "CREATE TYPE ck_salmonella_subspecies AS ENUM ('arizonae', 'indica', 'enterica', 'houtenae', 'diarizonae', 'salamae');")
    op.execute("ALTER TABLE genome ADD COLUMN subspecies ck_salmonella_subspecies;")
    op.execute("UPDATE genome SET subspecies = _old_subspecies::text::ck_salmonella_subspecies;")
    op.execute("ALTER TABLE genome DROP COLUMN _old_subspecies;")
    op.execute("DROP TYPE _old_ck_salmonella_subspecies;")