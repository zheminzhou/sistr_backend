#!.venv/bin/python
__author__ = 'piotr'

import argparse
import logging

import os
from app.models import User, Genome
from app import session
from app.config import Config
from app import logger
from app import logging_format
from app.tasks import refine_serovar_prediction, assign_cgmlst_clusters
from app.blast_wrapper import BlastRunner


prog_desc = '''
Predict the serovar of a genome and add the prediction to the SISTR DB
'''

parser = argparse.ArgumentParser(prog='predict_serovar',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=prog_desc)

parser.add_argument('-f',
                    '--fasta_path',
                    help='Input genome FASTA file (genome name in filename without extension - no periods in genome name!)')
parser.add_argument('-u',
                    '--user',
                    default=Config.PUBLIC_SISTR_USER_NAME,
                    help='Genome user to find genome under (default: "{}")'.format(Config.PUBLIC_SISTR_USER_NAME))
parser.add_argument('-T',
                    '--tmp_dir',
                    default='/tmp',
                    help='Base temporary working directory for intermediate analysis files.')
parser.add_argument('-v',
                    '--verbose',
                    action='count',
                    default=0)


def get_user(user_name):
    return session.query(User).filter_by(name=user_name).first()


def init_console_logger():
    global logging_verbosity, console_logger
    logging_verbosity = args.verbose
    console_logger = logging.StreamHandler()
    console_logger.setFormatter(logging_format)
    if logging_verbosity == 0:
        console_logger.setLevel(logging.ERROR)
    elif logging_verbosity == 1:
        console_logger.setLevel(logging.WARN)
    elif logging_verbosity == 2:
        console_logger.setLevel(logging.INFO)
    elif logging_verbosity == 3:
        console_logger.setLevel(logging.DEBUG)
    logger.addHandler(console_logger)


if __name__ == '__main__':
    args = parser.parse_args()

    init_console_logger()

    if args.fasta_path is None:
        print 'You need to specify a genome fasta path!'
        parser.print_help()
        exit(1)

    user = get_user(args.user)
    assert user is not None

    assert os.path.exists(args.fasta_path)

    fasta_filename = os.path.basename(args.fasta_path)
    genome_name = fasta_filename.split('.')[0]
    logger.debug('Genome name {} from filename {}'.format(genome_name, fasta_filename))

    genome = session.query(Genome).filter(Genome.user == user, Genome.name == genome_name).first()
    br = BlastRunner(args.fasta_path, os.path.join(args.tmp_dir, genome_name))
    logger.info('Initializing working directory and preparing for antigen BLAST searching.')
    br.prep_blast()
    tmp_fasta_path = br.tmp_fasta_path
    logger.info('Temporary FASTA file copied to {}'.format(tmp_fasta_path))
    assert genome is not None
    if genome.cgmlst_clusters is None or len(genome.cgmlst_clusters) == 0:
        logger.warning(
            'No cgMLST clusters for genome {} for user {}. Assigning tentative cgMLST clusters.'.format(genome_name,
                                                                                                        user.name))
        assign_cgmlst_clusters(temp_file_path=tmp_fasta_path,
                               user_name=user.name,
                               genome_name=genome_name)

    logger.info('Predicting serovar based on cgMLST cluster membership analysis and antigen BLAST results.')
    refine_serovar_prediction(temp_file_path=tmp_fasta_path,
                              genome_name=genome_name,
                              user_name=user.name)

    logger.info('Deleting temporary working directory at {}'.format(br.tmp_work_dir))
    br.cleanup()
