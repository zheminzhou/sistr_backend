===========
Development
===========




Tasks
======

.. automodule:: app.tasks
:members:
        :undoc-members:
        :private-members:
        :special-members:


Parsers
=========

.. automodule:: app.parsers
:members:
        :undoc-members:
        :private-members:
        :special-members:


.. [QUAST]
    Alexey Gurevich, Vladislav Saveliev, Nikolay Vyahhi and Glenn Tesler,
    QUAST: quality assessment tool for genome assemblies,
    Bioinformatics (2013) 29 (8): 1072-1075.
    doi: 10.1093/bioinformatics/btt086
    First published online: February 19, 2013.
    http://bioinf.spbau.ru/quast

.. [MIST]
    MicrobialInSilicoTyper (MIST). 
    https://bitbucket.org/peterk87/microbialinsilicotyper