=====================================================
SISTR RESTful Application Programming Interface (API)
=====================================================

The SISTR server app exposes a REST API for allowing other applications to
send and receive data from SISTR.
The SISTR web application communicates with the SISTR server app through this REST API.

.. note::

    All routes return data in JSON format.

Routes
======

All REST API routes are declared in :mod:`app.rest`.

MIST *in silico* typing tests
-----------------------------

Get information about an *in silico* MIST typing test

``/api/mist_test/<string:mist_test_name>``

or about all typing tests:

``/api/mist_tests``

