===========================
SISTR-backend documentation
===========================

The `Salmonella In Silico Typing Resource (SISTR) <http://lfz.corefacility.ca/sistr-app/>`_ is a web application for the rapid *in silico* typing and *in silico* serovar prediction of *Salmonella enterica* isolates using whole-genome sequence (WGS) data.
In addition, it allows various metadata-driven comparative genomic and epidemiological analyses through the `SISTR web application <https://bitbucket.org/peterk87/sistr-app>`_.

SISTR consists of a `Python (v2.7.9) <https://www.python.org/>`_ server application and `PostgreSQL (v9.4.1) <http://www.postgresql.org/>`_ database and a `ClojureScript (v0.0-3269) <https://github.com/clojure/clojurescript>`_ web application. 
The server app is implemented in Python using the `Flask web micro web framework (v0.10.1) <http://flask.pocoo.org/>`_ and communicates with the PostgreSQL database using `SQLAlchemy (v1.0.2) <http://www.sqlalchemy.org/>`_.
The server app exposes a REST API through which the user facing `SISTR web application <https://bitbucket.org/peterk87/sistr-app>`_ sends and receives data. 
It is also possible for other applications to send data to and from the server app through the REST API.
A `Celery distributed task queue (v3.1.18) <https://celery.readthedocs.org/en/latest/index.html>`_ is used for asynchronously running tasks such as in silico analyses on user uploaded genomes.

The source code for SISTR-backend is available on Bitbucket at:
https://bitbucket.org/peterk87/sistr_backend

This documentation is for the SISTR backend server application.
The documentation for the SISTR web application can be found at 
https://bitbucket.org/peterk87/sistr-app

Contents:
=========

.. toctree::
:maxdepth: 3

        getting_started
        serovar_prediction
        rmlst
        db_schema
        rest_api
        development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

