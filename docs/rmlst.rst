==============
rMLST in SISTR
==============

A 2015-01-30 version of the [rMLST]_ scheme was adapted for use within SISTR.

The following rMLST markers were removed since they contained `N`s in the rMLST profiles table:

.. code-block:: text

    BACT000014
    BACT000018
    BACT000021
    BACT000035
    BACT000050
    BACT000057
    BACT000059
    BACT000060
    BACT000062
    BACT000065

It's not obvious what these `N`s mean in the context of this table.


The following rMLST markers were removed due to sharing alleles with the 330 marker cgMLST used in SISTR:

.. code-block:: text

    BACT000016
    BACT000061



Finding a matching rMLST profile
================================

[MIST]_ can only find perfect matching profiles to any in silico assay results so it was necessary to implement functions to find the closest matching profiles given a set of rMLST results.
Also, the rMLST table contains many rows of rMLST profiles with incomplete genus/species information.
Rows with genus and species info missing are filtered out before finding a closest matching rMLST profile in the table from the in silico rMLST results.

This functionality is implemented in:


.. autofunction:: app.tasks.interpret_rmlst


References
==========

.. [rMLST]
    Ribosomal Multilocus Sequence Typing (rMLST). 
    http://pubmlst.org/rmlst/

.. [MIST]
    MicrobialInSilicoTyper (MIST). 
    https://bitbucket.org/peterk87/microbialinsilicotyper
