from __future__ import absolute_import

from datetime import datetime
from app.genome_metadata import get_genome_metadata_dict, get_basic_serovar_prediction_info, get_genome_quality_stats
from flask import g, current_app
from flask.ext.restful import reqparse, Resource

from app import session, logger, cache
from app.api import authorized, auth
from app.models import User, UserRole, Genome

__author__ = 'piotr'


class UserCheckAPI(Resource):
    def get(self, user_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user:
            return {}, 200
        else:
            return {}, 404


class UserTokenAPI(Resource):
    @auth.login_required
    def get(self):
        sk = current_app.config['SECRET_KEY']
        token = g.user.generate_auth_token(sk)
        logger.info('Get Auth token for user "{}"'.format(g.user))
        return {'token': token.decode('ascii'),
                'duration': 86400}


class UserAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('password', type=str, location='json')
        self.reqparse.add_argument('email', type=str, location='json')
        self.reqparse.add_argument('new_username', type=str, location='json')
        super(UserAPI, self).__init__()

    @staticmethod
    def user_info(user):
        assert isinstance(user, User)
        genome_name_query = session.query(Genome.name)\
            .filter(Genome.user_id == user.id,
                    Genome.is_analyzed)
        genome_names = [x for x, in genome_name_query.all()]
        return {'name': user.name,
                'last_seen': user.last_seen.isoformat(),
                'genomes': genome_names,
                'role': user.role.value,
                'email': user.email}

    @authorized(public_auth_required=False)
    def get(self, user_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        assert isinstance(user, User)
        logger.info('GET User {}; username {}'.format(user, user_name))
        # Update when User last seen
        user.last_seen = datetime.utcnow()
        session.add(user)
        session.commit()
        return UserAPI.user_info(user)


    def post(self, user_name):
        """
        If the user makes a POST request with a new user name then try to fetch
        that user.
        If a user by that name does not exist then create one.
        If the user is POSTing with the public SISTR user name then create a
        new randomly named user.
        @param user_name: User name of new user to create/fetch. If public
        SISTR user name then generate a new random user name.
        @return: New/existing non-public user info.
        """
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name == public_username:
            user_name = User.create_new_user_id(session)

        logger.info('Creating new user "{}"'.format(user_name))
        user = session.query(User).filter(User.name == user_name).first()
        logger.info('Query for user "{}"={}'.format(user_name, user))
        # registered users cannot be retrieved by POST; neeed auth to GET registered user info
        if user is not None and user.role == UserRole.registered:
            return {'error': 'User already exists. Login required.'}, 403
            # temporary users can be retrieved by POST; no auth needed to retrieve info
        if user is not None and user.role == UserRole.temporary:
            # update user's last seen
            user.last_seen = datetime.utcnow()
            session.add(user)
            session.commit()
            return UserAPI.user_info(user), 200

        # TODO: check if user_name is valid; no whitespace, no special characters, regex matches ^\w+$

        user = User(name=user_name, last_seen=datetime.utcnow())
        args = self.reqparse.parse_args()
        password = args['password']
        email = args['email']
        resp = {}
        # if new user provides password, then hash password and set UserRole to registered
        if password:
            logger.info('New user {} has password; setting UserRole to Registered user'.format(user_name))
            user.hash_password(password)
            user.role = UserRole.registered
            if email:
                # TODO: check if user email is valid
                user.email = email

        session.add(user)
        session.commit()
        resp['name'] = user.name
        resp['last_seen'] = user.last_seen.isoformat()
        resp['role'] = user.role.value

        if user.role == UserRole.registered:
            resp['token'] = user.generate_auth_token(current_app.config['SECRET_KEY'])
            # user = session.query(User).filter(User.name == user.name).first()

        if user.email:
            resp['email'] = user.email

        return resp, 201

    @authorized(public_auth_required=True)
    def put(self, user_name):
        '''
        Update User_ information.
        '''
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('PUT User "{}" does not exist'.format(user_name))
            return {'error': 'User not found'}, 404

        if user != g.user:
            err_msg = 'Specified user does not match authenticated user'
            logger.error(err_msg)
            return {'error': err_msg}, 403

        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        args = self.reqparse.parse_args()
        password = args['password']
        email = args['email']
        new_username = args['new_username']
        logger.info('PUT user_name="{}"; new password?={}; email="{}"; new_username="{}"'.format(
            user_name,
            password is not None,
            email,
            new_username))

        if new_username:
            if public_username == user_name:
                return {'error': 'Renaming this user not permitted'}, 403
            if session.query(User).filter(User.name == new_username).count() > 0:
                return {'error': 'Name "{}" already taken. Please try another name'.format(new_username)}, 403
            user.name = new_username
            session.add(user)
            session.commit()

            #: Invalidate cached metadata, serovar prediction results and quality stats info for all User genomes
            for genome in user.genomes.all():
                cache.delete_memoized(get_genome_metadata_dict, genome.id)
                cache.delete_memoized(get_basic_serovar_prediction_info, genome.id)
                cache.delete_memoized(get_genome_quality_stats, genome.id)

        if email:
            if public_username == user_name:
                return {'error': 'Cannot change email address for this user'}, 403

            # TODO: spruce up very basic email validation
            import re

            if not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email):
                return {'error': 'Invalid email address "{}"'.format(email)}, 403

            user.email = email
            session.add(user)
            session.commit()

        if password:
            if public_username == user_name:
                # TODO: password change of public SISTR user allowed through API?
                return {'error': 'Cannot change password for this user'}, 403

            # TODO: more password validation
            if len(password) < 6:
                return {'error': 'Passwords must be at least 6 characters in length'}, 403
            user.hash_password(password)
            user.role = UserRole.registered
            session.add(user)
            session.commit()

        resp = UserAPI.user_info(user)
        if user.role == UserRole.registered:
            resp['token'] = user.generate_auth_token(current_app.config['SECRET_KEY'])
        return resp


class UserSelectionsAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('selections', type=dict, location='json')

    @authorized(public_auth_required=False)
    def get(self, user_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        assert isinstance(user, User)
        return user.selections

    @authorized(public_auth_required=True)
    def put(self, user_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        assert isinstance(user, User)

        args = self.reqparse.parse_args()
        selections = args['selections']
        if selections is None:
            # return User selections
            return user.selections, 200

        # TODO: merge selections if they are different so that nothing is ever overwritten?
        user.selections = selections
        session.add(user)
        session.commit()

        return user.selections, 200


class UserPasswordResetAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email')
        self.reqparse.add_argument('username')
        super(UserPasswordResetAPI, self).__init__()

    @staticmethod
    def reset_password_send_email(user):
        email_subject_fmt = '[SISTR] Password Reset for user "{user}"'
        email_fmt = (
            'SISTR - Salmonella In Silico Typing Resource\n'
            '\n'
            'Password for user "{user}" has been reset!\n'
            '\n'
            'Username: {user}\n'
            'Temporary Password: {pw}\n'
            '\n'
            'Do not reply to this email.'
        )
        temp_pw = User.create_new_user_id(session)
        user.hash_password(temp_pw)
        session.add(user)
        session.commit()
        from ...tasks import send_email

        t = send_email.s([user.email],
                         email_subject_fmt.format(user=user.name),
                         email_fmt.format(user=user.name,
                                          pw=temp_pw))
        r = t.apply_async()
        logger.info('Added task ({}) to send email to {} for user {}'.format(r, user.email, user))

    def get(self):
        args = self.reqparse.parse_args()
        logger.info('Parsed args {} for password reset'.format(args))
        email = args['email']
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if email:
            users = session.query(User).filter(User.email == email).all()
            for user in users:
                if user.name == public_username:
                    logger.warning('Attempt to reset password for public SISTR user!')
                    continue
                UserPasswordResetAPI.reset_password_send_email(user)

        username = args['username']
        if username:
            if username == public_username:
                logger.warning('Attempt to reset password for public SISTR user!')
            else:
                user = session.query(User).filter(User.name == username).first()
                if user and user.email:
                    UserPasswordResetAPI.reset_password_send_email(user)

        return {}, 200
