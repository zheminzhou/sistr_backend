from __future__ import absolute_import

from datetime import datetime
from flask import g, current_app
import pandas as pd
from flask.ext.restful import Resource, reqparse, abort
from sqlalchemy import or_
from werkzeug.datastructures import FileStorage

from ...db_loaders import DefaultDataLoader
from ... import logger, session, cache
from .. import authorized

from ...genome_metadata import get_genome_metadata_dict, listdict_to_dictlist, genome_assembly_stats, get_genome_quality_stats, get_basic_serovar_prediction_info, user_genomes_metadata, genome_locations
from ...models import Genome, User, SerovarPrediction, CurationInfo

from ...tasks import analyse_genome

__author__ = 'piotr'


class GenomeAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('fasta', type=FileStorage, location='files')
        self.reqparse.add_argument('metadata', type=dict, location='json')
        self.reqparse.add_argument('run_rmlst', type=bool, default=False)

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        user = g.user
        if not user:
            return {'error': 'user not found'}, 404

        genome = user.genomes.filter(Genome.name == genome_name).first()

        if genome is None:
            return abort(404)
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403

        return {'metadata': get_genome_metadata_dict(genome),
                'user': user_name,
                'genome': genome_name}

    @authorized(public_auth_required=True)
    def post(self, user_name, genome_name):
        user = g.user
        if user is None:
            return {'error': 'user "{0}" does not exist'.format(user_name)}, 404

        if user_name != user.name:
            return {'error': 'specified username "{}" does not match existing username'.format(user_name)}, 404

        args = self.reqparse.parse_args()
        fasta = args['fasta']
        if fasta is None:
            return abort(400)

        genome_name = Genome.make_unique_name(genome_name, session)

        import tempfile, os
        fd, filename = tempfile.mkstemp()
        tfile = os.fdopen(fd, 'w')
        tfile.write(fasta.read())
        tfile.close()

        task = analyse_genome(current_app,
                               fasta_temp_file=filename,
                               user_name=user_name,
                               genome_name=genome_name,
                               run_rmlst=args['run_rmlst'])

        return {'task': task.id,
                'user': user_name,
                'genome': genome_name}, 201


class GenomesAssemblyStatsAPI(Resource):
    @authorized(public_auth_required=False)
    @cache.memoize(60)
    def get(self, user_name):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_stats = genome_assembly_stats(public_username)
        if user_name == public_username:
            return listdict_to_dictlist(public_stats)

        private_stats = genome_assembly_stats(user_name)

        return listdict_to_dictlist(private_stats + public_stats)


class GenomeMetadataAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('metadata', type=dict, location='json')
        self.reqparse.add_argument('curation_code', type=str, location='json')
        super(GenomeMetadataAPI, self).__init__()

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        genome_query = session.query(Genome) \
            .join(Genome.user) \
            .filter(or_(User.name == public_username, User.name == user_name)) \
            .filter(Genome.name == genome_name)

        genome = genome_query.first()
        if genome is None:
            return None
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403
        genome_metadata_dict = get_genome_metadata_dict(genome.id)
        return genome_metadata_dict

    @authorized(public_auth_required=True)
    def put(self, user_name, genome_name):
        user = g.user

        if user is None:
            return abort(404)

        genome = session.query(Genome).join(Genome.user) \
            .filter(User.name == user_name) \
            .filter(Genome.name == genome_name) \
            .first()

        if genome is None:
            return abort(404)
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403

        args = self.reqparse.parse_args()
        logger.info('PUT args {}'.format(args))
        metadata = args['metadata']
        curation_code = args['curation_code']
        if metadata is None and curation_code is None:
            logger.info('PUT user {} genome {} no metadata or curation code'.format(user, genome))
            return {'metadata': get_genome_metadata_dict(genome.id),
                    'user': user_name,
                    'genome': genome_name}, 204

        if metadata:
            logger.info('PUT user {} genome {} metadata {}'.format(user, genome, metadata))
            df = pd.DataFrame({genome_name: metadata}).transpose()
            if 'collection_date' in df.columns:
                df.collection_date = pd.to_datetime(df.collection_date).astype(datetime)
            df.rename(columns=DefaultDataLoader.HOST_FIELD_DICT, inplace=True)
            ddl = DefaultDataLoader(session)
            ddl.parse_genome_metadata(df, user)

        if curation_code:
            logger.info('PUT user {} genome {} curation code {}'.format(user, genome, curation_code))
            ci = session.query(CurationInfo).filter(CurationInfo.code == curation_code).first()
            if ci:
                assert isinstance(ci, CurationInfo)
                genome.curation_info_id = ci.id
                session.add(genome)
                session.commit()

        genome = session.query(Genome).join(Genome.user) \
            .filter(User.name == user_name) \
            .filter(Genome.name == genome_name) \
            .first()

        #: Invalidate cached metadata, serovar prediction results and quality stats info for Genome
        cache.delete_memoized(get_genome_metadata_dict, genome.id)
        cache.delete_memoized(get_basic_serovar_prediction_info, genome.id)
        cache.delete_memoized(get_genome_quality_stats, genome.id)

        return {'metadata': get_genome_metadata_dict(genome.id),
                'user': user_name,
                'genome': genome_name}, 200


class GenomeMetadataListAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_md = user_genomes_metadata(public_username)
        if user_name == public_username:
            return listdict_to_dictlist(public_md)
        user = session.query(User).filter(User.name == user_name).scalar()
        if user is None:
            return listdict_to_dictlist(public_md)
        if user.genomes.count() == 0:
            return listdict_to_dictlist(public_md)
        private_md = user_genomes_metadata(user_name)
        public_private_md = public_md + private_md
        return listdict_to_dictlist(public_private_md)


class GenomeMetadataListObjAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_md = user_genomes_metadata(public_username)

        if user_name == public_username:
            return public_md
        user = session.query(User).filter(User.name == user_name).scalar()
        if user is None:
            return public_md
        if user.genomes.count() == 0:
            return public_md
        private_md = user_genomes_metadata(user_name)
        return public_md + private_md


class GenomesLocationsAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_locations = genome_locations(public_username)
        if user_name == public_username:
            return public_locations

        private_locations = genome_locations(user_name)
        return private_locations + public_locations


class GenomeSerovarPredictionAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        user = session.query(User).filter(User.name == user_name).scalar()
        if user is None:
            return {'error': 'No user "{}" exists!'.format(user_name)}
        genome = session.query(Genome).filter(Genome.user == user, Genome.name == genome_name).first()
        if genome is None:
            return {'error': 'No such genome "{}" for user "{}"'.format(genome_name, user_name)}
        return get_basic_serovar_prediction_info(genome.id)


class GenomeSerovarPredictionsListAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        user = session.query(User).filter(User.name == user_name).scalar()
        if user is None:
            return {'error': 'No user "{}" exists!'.format(user_name)}
        if user.genomes.count() == 0:
            logger.info('User {} has no genomes'.format(user))
            return {}
        genome_query = session.query(Genome.id) \
            .filter(Genome.user_id == user.id, Genome.is_analyzed)
        d = [get_basic_serovar_prediction_info(genome_id) for genome_id, in genome_query.all()]
        return listdict_to_dictlist(d)


class SerovarCurationInfoAPI(Resource):
    @cache.memoize(int(1e7))
    def get(self):
        curation_infos = session.query(CurationInfo).all()

        d = []
        for curation_info in curation_infos:
            d.append({'is_trusted': curation_info.is_trusted,
                      'code': curation_info.code,
                      'description': curation_info.description})

        return listdict_to_dictlist(d)


class GenomeCurationInfoListAPI(Resource):
    @cache.memoize(int(1e7))
    def get(self):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        user = session.query(User).filter(User.name == public_username).scalar()
        d = []
        for genome in user.genomes.all():
            assert isinstance(genome, Genome)
            curation_info = genome.curation_info
            assert isinstance(curation_info, CurationInfo)

            reported_serovar = 'reported_serovar'
            reported_serovar = genome.misc_metadata[reported_serovar] \
                if genome.misc_metadata is not None \
                and reported_serovar in genome.misc_metadata \
                else ''
            serovar_prediction = genome.serovar_prediction.first()
            assert isinstance(serovar_prediction, SerovarPrediction)
            d.append({'genome': genome.name,
                      'user_uploader': user.name,
                      'is_trusted': curation_info.is_trusted,
                      'code': curation_info.code,
                      'reported_serovar': reported_serovar,
                      'curated_serovar': genome.serovar,
                      'predicted_serovar': serovar_prediction.serovar})
        return listdict_to_dictlist(d)
