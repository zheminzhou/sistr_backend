from __future__ import absolute_import

from .. import authorized
from flask import g, current_app

__author__ = 'piotr'
from app import cache, session, logger
from app.cgmlst import find_closest_related_genome, condensed_dist_matrix, cgmlst_profiles
from app.models import User, Genome, CurationInfo
from networkx.convert_matrix import from_numpy_matrix
from flask.ext.restful import Resource, reqparse
import numpy as np
# TODO: compare networkx to graph-tool for resolving performance issues with large matrices/graphs
from networkx import minimum_spanning_tree
from networkx.readwrite import json_graph
from scipy.cluster.hierarchy import to_tree
from ete2 import Tree
# using linkage from fastcluster library for O(n^2) clustering
# vs O(n^3) clustering with scipy.cluster.hierarchy linkage function
from fastcluster import linkage
from scipy.spatial.distance import squareform


def complete_linkage(dm):
    """
    Perform complete linkage hierarchical clustering on a distance matrix.

    Args:
        dm (numpy.array): Distance matrix

    Returns:
        (object): fastcluster complete linkage hierarchical clustering object
    """
    return linkage(dm, 'complete')


@cache.memoize(timeout=int(1e7))
def adjacency_list(genomes, user_name):
    dm, gs_collapse = condensed_dist_matrix(genomes)
    logger.info('Retrieved condensed distance matrix of size {0} for user {1}'.format(dm.shape,
                                                                                      user_name))
    logger.info('Distance matrix of size {} to graph object for user {}'.format(dm.shape, user_name))
    g = from_numpy_matrix(dm)
    logger.info('Graph object (nodes={}, edges={}) to MST for user {}'.format(g.number_of_nodes(),
                                                                              g.number_of_edges(),
                                                                              user_name))
    mst = minimum_spanning_tree(g)
    logger.info('MST graph object of size {} to JSON adjacency list for user {}'.format(mst.number_of_nodes(),
                                                                                        user_name))
    node_links = json_graph.node_link_data(mst)
    rtn_obj = {'genomes': gs_collapse, 'mst': node_links}
    return rtn_obj


def get_user_accessible_genomes(user_name):
    """
    Get a list of Genome_ names accessible to a specified User.

    Args:
        user_name (str): User_ name

    Returns:
        list: Genome_ names
    """
    public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
    logger.info(
        'No genomes specified for cgMLST tree. Getting all accessible genomes for user {}'.format(user_name))
    public_genomes = [name for name, in session.query(Genome.name) \
        .join(Genome.user) \
        .filter(User.name == public_username).all()]
    if user_name == public_username:
        genomes = public_genomes
    else:
        private_genomes = [name for name, in session.query(Genome.name) \
            .join(Genome.user) \
            .filter(User.name == user_name).all()]
        genomes = private_genomes + public_genomes
    logger.info('Getting all tree for all {} genomes for user {}'.format(len(genomes), user_name))
    return genomes


@cache.memoize(timeout=int(1e7))
def get_newick_tree(genomes):
    """
    Construct a complete linkage hierarchical clustering tree for some specified Genomes_ and return
    the Newick string for that tree along with a list of the list of Genomes_ at each leaf node.

    Args:
        genomes (list): List of Genome_ names

    Notes:
        cgMLST_330 profiles are expected to stay static after initial analysis of a Genome_ so the profiles, trees, distance matrices, etc derived from the cgMLST results can be cached essentially forever.

    Returns:
        dict['tree']: Newick string of collapsed tree of specified `genomes`
        dict['genomes']: 2D list of genomes grouped based on 100% cgMLST profile similarity
    """
    dm, gs_collapse = condensed_dist_matrix(genomes)
    logger.info(
        'Distance matrix has {} nodes from {} genomes; shape {}'.format(len(gs_collapse), len(genomes), dm.shape))
    # Thank you jhc for the tree building code
    # https://stackoverflow.com/questions/9364609/converting-ndarray-generated-by-hcluster-into-a-newick-string-for-use-with-ete2/17657426#17657426
    # use Scipy for complete linkage hierarchical clustering
    # squareform on a MxM matrix converts it to a (M x (M-1) / 2)
    # upper triangular 1D array of non-redundant distances
    # TODO: cgMLST profile distance func returns this array directly
    dm_triu = squareform(dm)
    logger.warn('dm_triu shape {}'.format(dm_triu.shape))
    Z = complete_linkage(dm_triu)
    logger.info(
        'complete linkage clustering performed; complete linkage {}; dist matrix shape {}'.format(Z.shape, dm.shape))
    T = to_tree(Z)
    logger.info('linkage to tree; T {}'.format(T))
    # use ete2 for creating the Newick tree
    root = Tree()
    root.dist = 0
    root.name = "root"
    item2node = {T: root}
    logger.info('Constructing tree graph for {} genomes'.format(len(gs_collapse)))
    to_visit = [T]
    while to_visit:
        node = to_visit.pop()
        cl_dist = node.dist / 2.0
        for ch_node in [node.left, node.right]:
            if ch_node:
                ch = Tree()
                ch.dist = cl_dist
                ch.name = str(ch_node.id)
                item2node[node].add_child(ch)
                item2node[ch_node] = ch
                to_visit.append(ch_node)
    tree = root
    # rename the leaf nodes from index to genome names
    for lf in tree.iter_leaves():
        idx = int(lf.name)
        try:
            lf.name = gs_collapse[idx][0]
        except Exception as e:
            logger.fatal(
                'Exception: "{}". Failed to rename tree leaf. Length of genome groups {}; index {}; leaf {}; leaf name {}'.format(
                    e.message, len(gs_collapse), idx, lf, lf.name))
            # ladderize to make the tree look a little nicer
    logger.info('Ladderizing tree')
    tree.ladderize()
    logger.debug('Tree graph constructed; tree {}'.format(tree))
    logger.info('Tree ladderized. Returning newick string of tree.')
    rtn_obj = {'tree': tree.write(), 'genomes': gs_collapse}
    return rtn_obj


class CgMLSTProfilesAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        genomes = get_user_accessible_genomes(user_name)
        return cgmlst_profiles(genomes)


class CgMLSTDistanceMatrixAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get cgMLST distance matrix
        ==========================

        Get the pairwise non-redundant distances between wgMLST_330 *in silico*
        derived profiles for genomes accessible to a user.
        The lower triangular distance matrix values are returned as a 1-D list.
        This requires that the client rearrange the values into the appropriate
        configuration.

        Args:
            user_name: user name

        Returns:
            dict: pairwise non-redundant wgMLST_330 distances between genomes
                corresponding to values in the lower triangle of square distance
                matrix.

        Notes:
            Genomes with 0 distance between each other will be grouped together
            under ``genomes`` and will only have one distance in the distance
            matrix.
            This along with only returning the distances for the lower triangular
            matrix are done to save on bandwidth.
        """
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name is None:
            user_name = public_username
        genomes = get_user_accessible_genomes(user_name)
        dm, gs_collapse = condensed_dist_matrix(genomes)

        # only return the values belonging to the lower triangular matrix
        il = np.tril_indices(dm.shape[0])
        return {'genomes': gs_collapse, 'distances': list(dm[il])}


class CgMLSTAdjacencyList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('genome', type=list, location='json')
        super(CgMLSTAdjacencyList, self).__init__()

    @authorized(public_auth_required=False)
    def post(self, user_name):
        args = self.reqparse.parse_args()
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name is None:
            user_name = public_username
        logger.info('Getting MST adjacency list for genomes accessible to user {}'.format(user_name))

        genomes = args['genome']
        if genomes is None:
            genomes = get_user_accessible_genomes(user_name)

        rtn_obj = adjacency_list(genomes, user_name)

        return rtn_obj

    @authorized(public_auth_required=False)
    def get(self, user_name):
        return self.post(user_name)


class CgMLSTNewickTree(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('genome', type=list, location='json')
        super(CgMLSTNewickTree, self).__init__()

    @authorized(public_auth_required=False)
    def post(self, user_name):
        args = self.reqparse.parse_args()
        logger.info('parsing args {}'.format(args.keys()))

        genomes = args['genome']

        if genomes is None:
            genomes = get_user_accessible_genomes(user_name)
        else:
            logger.info('Getting Newick tree with {} genomes'.format(len(genomes)))

        rtn_obj = get_newick_tree(genomes)
        return rtn_obj

    @authorized(public_auth_required=False)
    def get(self, user_name):
        return self.post(user_name)


class ClosestRelative(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('curated_only', type=bool, default=False)
        super(ClosestRelative, self).__init__()

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        args = self.reqparse.parse_args()
        curated_only = args['curated_only']

        logger.info('Retrieving closest relative (curated={}) for genome {} of user {}'.format(
            curated_only,
            genome_name,
            user_name))
        user = g.user
        if user is None:
            return {'error': 'user {0} not found'.format(user_name)}, 404
        genome = user.genomes.filter(Genome.name == genome_name).first()
        if genome is None:
            return {'error': 'genome {0} not found'.format(genome)}, 404
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome)}, 403

        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_genome_names = [name for name, in session.query(Genome.name) \
            .join(Genome.user) \
            .join(CurationInfo) \
            .filter(User.name == public_username,
                    Genome.is_analyzed,
                    CurationInfo.is_trusted == curated_only) \
            .order_by(Genome.id) \
            .all()]
        public_genome_profiles = cgmlst_profiles(public_genome_names)
        closest_relative, genome_matching_alleles = find_closest_related_genome(genome_name,
                                                                                public_genome_profiles)
        logger.info(
            'GET - The closest public genome relative for genome {0} of user {1} is {2} with {3} matching alleles.'.format(
                genome_name,
                user_name,
                closest_relative['genome'],
                closest_relative['n_matching_alleles']))
        return {'closest_relative': closest_relative,
                'relatives': genome_matching_alleles,
                'genome': genome_name,
                'user': user_name}
