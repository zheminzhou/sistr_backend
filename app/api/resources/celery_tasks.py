from __future__ import absolute_import
from app import logger

from flask.ext.restful import Resource, reqparse
from redis import Redis

__author__ = 'piotr'


class CeleryTaskAPI(Resource):
    """
    Celery Task API
    ===============

    API for getting the status of one or more Celery tasks.
    """

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('task_id', type=str, action='append', required=True)
        super(CeleryTaskAPI, self).__init__()

    def get(self):
        """
        Get the status of one or more Celery tasks.
        The ``task_id`` request argument should contain one or more task IDs.

        Returns:
            (list): list of dicts of task IDs and statuses
        """
        args = self.reqparse.parse_args()
        task_ids = args['task_id']
        rtn = []
        from ...tasks import CELERY

        for task_id in task_ids:
            task = CELERY.AsyncResult(task_id)
            logger.info('task {} {}'.format(task, task.__dict__))
            rtn.append({
                'task_id': task.task_id,
                'status': task.status,
                'info': task.info,
                'result': task.result,
                })
        return rtn

class CeleryQueueMonAPI(Resource):

    def get(self):
        r = Redis()
        celery_queue_name = 'celery'
        tasks_queued = r.llen(celery_queue_name)

        resp_dict = {'tasks_queued': tasks_queued, }

        try:
            timings_key = 'genome_analysis_task_timings'
            timings = [float(x) for x in r.lrange(timings_key, 0, r.llen(timings_key))]

            import numpy

            mean_runtime = numpy.mean(timings)
            median_runtime = numpy.median(timings)
            std_runtime = numpy.std(timings)
            resp_dict['runtime_stats'] = {'mean': mean_runtime,
                                          'median': median_runtime,
                                          'std': std_runtime,}
        except:
            pass

        return resp_dict