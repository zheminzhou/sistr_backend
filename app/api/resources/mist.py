from collections import defaultdict
from app import session, cache
from app.models import MistTest, User, Genome, MistMarkerResult, MistMetadataResult, MistMarker
from flask import current_app
from flask.ext.restful import fields, Resource, marshal, marshal_with, abort
from sqlalchemy import or_

__author__ = 'piotr'


class DBEnumTypeItem(fields.Raw):
    def format(self, value):
        return value.value


class MarkerNameItems(fields.Raw):
    def format(self, value):
        return [m.name for m in value.all()]


mist_test_fields = {
    'name': fields.String,
    'type': DBEnumTypeItem,
    'markers': MarkerNameItems
}


class MistTestListAPI(Resource):
    def get(self):
        mist_tests = session.query(MistTest).all()
        map_res = map(lambda t: marshal(t, mist_test_fields), mist_tests)
        return {'tests': map_res}


class MistTestAPI(Resource):
    @cache.memoize(30)
    @marshal_with(mist_test_fields)
    def get(self, mist_test_name):
        mist_test = session.query(MistTest).filter(MistTest.name == mist_test_name).first()
        if mist_test is None:
            return abort(404)
        return mist_test


class MistTestResultItems(fields.Raw):
    def format(self, value):
        d = {}
        for m in value.all():
            d[m.test.name] = m.attrs

        return d


mist_marker_result_fields = {
    'result': fields.String,
    'is_missing': fields.Boolean,
    'is_contig_truncated': fields.Boolean,
    'has_potential_homopolymer_errors': fields.Boolean,
    'time_added': fields.DateTime(attribute='timestamp')
}


class MistMarkerResultItem(fields.Raw):
    def format(self, value):
        d = defaultdict(defaultdict)
        for m in value.all():
            d[m.test.name][m.marker.name] = marshal(m, mist_marker_result_fields)
        return d


genome_mist_results_fields = {
    'genome': fields.String(attribute='name'),
    'test_results': MistTestResultItems(attribute='mist_metadata_results'),
    'marker_results': MistMarkerResultItem(attribute='mist_marker_results')
}


class GenomeMistResultsAPI(Resource):
    @marshal_with(genome_mist_results_fields)
    @cache.memoize(120)
    def get(self, user_name, genome_name):
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        genome = session.query(Genome).join(User) \
            .filter(or_(User.name == public_username, User.name == user_name)) \
            .filter(Genome.name == genome_name) \
            .first()

        if genome is None:
            return abort(404)

        return genome


def get_mist_marker_results(user_name):
    query_stmt = session.query(Genome.name, MistTest.name, MistMarker.name, MistMarkerResult.result) \
        .join(Genome.mist_marker_results) \
        .join(MistMarkerResult.test) \
        .join(MistMarkerResult.marker) \
        .join(Genome.user) \
        .filter(User.name == user_name)

    d = defaultdict(dict)

    for g, t, m, r in query_stmt.all():
        if t in d[g]:
            d[g][t].append({m: r})
        else:
            d[g][t] = [{m: r}]

    return d


class GenomesMistResultsAPI(Resource):
    def get(self, user_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            return {'error': 'user {0} does not exist'.format(user_name)}, 404
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name == public_username:
            return get_mist_marker_results(public_username)
        else:
            return get_mist_marker_results(user_name)


class GenomeMistRawResultsAPI(Resource):
    @cache.memoize(30)
    def get(self, user_name, mist_test, genome_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            return abort(404)

        mist_test_obj = session.query(MistTest).filter(MistTest.name == mist_test).first()
        if mist_test_obj is None:
            return abort(404)

        genome = session.query(Genome).filter(Genome.name == genome_name).first()
        if genome is None:
            return abort(404)

        mist_results = session.query(MistMarkerResult) \
            .filter(MistMarkerResult.user_id == user.id) \
            .filter(MistMarkerResult.test_id == mist_test_obj.id) \
            .filter(MistMarkerResult.genome_id == genome.id) \
            .all()

        res = defaultdict(list)

        for mist_result in mist_results:
            mist_json = mist_result.mist_json
            for k, v in mist_json.iteritems():
                res[k].append(v)

        return {
            'user': user_name,
            'mist_test': mist_test,
            'genomes': genome_name,
            'mist_results': res
        }


class UserMistRawResultsListAPI(Resource):
    @cache.memoize(30)
    def get(self, user_name, mist_test):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            return abort(404)

        mist_test_obj = session.query(MistTest).filter(MistTest.name == mist_test).first()
        if mist_test_obj is None:
            return abort(404)

        mist_results = session.query(MistMarkerResult) \
            .filter(MistMarkerResult.user_id == user.id) \
            .filter(MistMarkerResult.test_id == mist_test_obj.id).all()

        res = defaultdict(list)
        genome_names = []
        for mist_result in mist_results:
            genome_name = mist_result.genome.name
            genome_names.append(genome_name)
            mist_json = mist_result.mist_json
            for k, v in mist_json.iteritems():
                res[k].append(v)

        return {
            'user': user_name,
            'mist_test': mist_test,
            'genomes': genome_names,
            'mist_results': res
        }


class UserGenomesMistTestMetadataListAPI(Resource):
    @cache.memoize(30)
    def get(self, user_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            return abort(404)
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']

        genome_mist_test_md = session.query(Genome.name, MistTest.name, MistMetadataResult.attrs) \
            .join(MistMetadataResult) \
            .join(MistTest) \
            .join(User) \
            .filter(or_(User.name == public_username, User.name == user_name)) \
            .all()

        test_md_list = [{'genome': genome, 'mist_test': test, 'test_results': results}
                        for genome, test, results in genome_mist_test_md]

        return test_md_list


class UserGenomesMistMarkerResultsAPI(Resource):
    def get(self, user_name, test_name):
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            return {'error': 'user {0} not found'.format(user_name)}, 404

        test = session.query(MistTest).filter(MistTest.name == test_name).first()
        if test is None:
            return {'error': 'MIST test {0} not found'.format(test_name)}, 404

        query_stmt = session.query(MistMarkerResult.result, Genome.name, MistMarker.name) \
            .join(MistMarkerResult.genome) \
            .join(MistMarkerResult.test) \
            .join(MistMarkerResult.marker) \
            .join(MistMarkerResult.user) \
            .filter(User.name == user_name) \
            .filter(MistTest.name == test_name)

        marker_results = [{'genome': genome, 'marker': marker, 'result': result}
                          for result, genome, marker in query_stmt.all()]

        return marker_results
