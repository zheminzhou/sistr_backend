from collections import defaultdict
from app import session, cache
from app.models import MistTest, MistMarkerResult, User, Genome, MistMarker, CurationInfo
from app.pairwise_distance import dist_matrix_with_nan
import numpy as np
import pandas as pd

__author__ = 'piotr'


def public_genomes_clusters_table(public_username):
    """
    Get clusters for public curated and trusted Genomes_.

    Returns:
        pandas.DataFrame: table of cgMLST clusters
    """
    public_user_id = session.query(User.id).filter(User.name == public_username).scalar()
    d = {name: clusters for name, clusters in
         session.query(Genome.name, Genome.cgmlst_clusters)
         .join(CurationInfo)
         .filter(Genome.user_id == public_user_id,
                 Genome.is_analyzed,
                 CurationInfo.is_trusted).all()}
    df = pd.DataFrame(d).transpose()
    return df


def find_closest_related_genome(genome_name, other_genome_profiles):
    """
    Find the curated and trusted public Genome_ which is most closely related to
    a specified Genome_ based on cgMLST profile similarity.

    Notes:
        It is assumed that ``other_genome_profiles`` contains only profiles for curated and trusted
        public Genomes_.
        This function is mostly used for cgMLST-based serovar prediction.

    Args:
        genome_name (str): Genome_ name
        other_genome_profiles (dict): Genome_ profiles to look through to find closest related Genomes_

    Returns:
        (dict, list): Most closely related Genome and list of other related Genomes_ in order of relatedness
    """
    genome = session.query(Genome).filter(Genome.name == genome_name).scalar()
    wgmlst_test_id = session.query(MistTest.id).filter(MistTest.name == "wgMLST_330").scalar()
    genome_profile = np.array([int(x.result) if x.result is not None else None for x in
                               genome.mist_marker_results.filter(
                                   MistMarkerResult.test_id == wgmlst_test_id).order_by(
                                   MistMarkerResult.marker_id).all()],
                              dtype=np.float64)
    other_genomes = other_genome_profiles['genomes']
    n_genomes = len(other_genomes)
    n_markers = len(other_genome_profiles['markers'])
    profiles_matrix = np.array(other_genome_profiles['results'], dtype=np.float64)
    profiles_matrix.shape = (n_genomes, n_markers)
    genome_profile_similarity_counts = np.apply_along_axis(lambda x: (x == genome_profile).sum(), 1, profiles_matrix)
    not_nan_counts = np.apply_along_axis(lambda x: np.sum((~np.isnan(x)) & (~np.isnan(genome_profile))), 1,
                                         profiles_matrix)
    genome_matching_alleles = []
    for other_genome_name, matching, non_nan_count in zip(other_genomes, genome_profile_similarity_counts,
                                                          not_nan_counts):
        if other_genome_name != genome_name:
            distance = 1.0 - (float(matching) / float(non_nan_count)) if float(non_nan_count) > 0 else 1.0
            genome_matching_alleles.append({
                'genome': other_genome_name,
                'n_matching_alleles': matching,
                'distance': distance,
                'non_missing_count': non_nan_count, })
    genome_matching_alleles.sort(key=lambda x: x['n_matching_alleles'], reverse=True)
    closest_relative = genome_matching_alleles[0]
    return closest_relative, genome_matching_alleles


@cache.memoize(timeout=int(1e7))
def condensed_dist_matrix(genome_names):
    """
    Get a condensed cgMLST pairwise distance matrix for specified Genomes_
    where condensed means redundant cgMLST profiles are only represented once in the distance matrix.

    Args:
        user_name (list): List of Genome_ names to retrieve condensed distance matrix for

    Returns:
        (numpy.array, list): tuple of condensed cgMLST distance matrix and list of grouped Genomes_
    """
    wgmlst_profiles = cgmlst_profiles(genome_names)
    results = wgmlst_profiles['results']
    genomes = wgmlst_profiles['genomes']
    n_genomes = len(genomes)
    n_markers = len(wgmlst_profiles['markers'])
    # put wgMLST values into a Numpy array
    arr = np.array(results, dtype=np.float64)
    # give the Numpy array the correct dimensions
    arr.shape = (n_genomes, n_markers)
    patterns = ['_'.join(['' if np.isnan(y) else str(int(y)) for y in arr[i, :]]) for i in range(n_genomes)]
    gs_collapse = []
    genome_idx_dict = {}
    indices = []
    patt_dict = {}
    for i, (g, p) in enumerate(zip(genomes, patterns)):
        if p in patt_dict:
            parent = patt_dict[p]
            idx = genome_idx_dict[parent]
            gs_collapse[idx].append(g)
        else:
            indices.append(i)
            patt_dict[p] = g
            genome_idx_dict[g] = len(gs_collapse)
            gs_collapse.append([g])
    arr = arr[indices, :]
    # calculate the distance matrix between genomes while taking NaN or
    # missing values into account
    # 1 - (# present / # union not NaN)
    dm = dist_matrix_with_nan(arr)
    return dm, gs_collapse


def dec_to_str(n):
    """
    Convert a decimal number into a string with only up to 2 decimal places.

    Args:
        n (float): Number to convert to string

    Returns:
        str: Decimal number string up to only 2 decimal places
    """
    x = int(100 * n) / 100.0
    if x == 0.0:
        return '0'
    return str(x)


def get_new_cluster_levels(cluster_levels, cluster_threshold):
    """
    Determine the cluster levels at which new cluster designations are required given
    a cluster distance threshold.

    Args:
        cluster_levels (list): List of possible cluster distance levels
        cluster_threshold (float): If float then converted to decimal string, if decimal string
          then must be within ``cluster_levels``

    Returns:
        list: List of cluster levels for which new cluster designations are required
    """
    if type(cluster_threshold) is str:
        if cluster_threshold not in cluster_levels:
            raise Exception(
                'Cluster level threshold is not in acceptable cluster levels "{}"'.format(cluster_threshold))
    if type(cluster_threshold) is float or type(cluster_threshold) is int:
        if cluster_threshold > 1.0 or cluster_threshold < 0.0:
            raise Exception(
                'Cluster level threshold outside acceptable range (not between 0 and 100 inclusive; cluster threshold is {})'.format(
                    cluster_threshold))
        cluster_threshold = dec_to_str(cluster_threshold)
    novel_levels = []
    for x in cluster_levels:
        if x != cluster_threshold:
            novel_levels.append(x)
        else:
            novel_levels.append(x)
            break
    return novel_levels

def generate_temp_cgmlst_clusters(df_cluster, genome, distance):
    """
    Generate temporary cgMLST clusters for a new genome based on distance to closest Genome with finalized
    cgMLST clusters. New clusters are temporarily assigned "-1" as a cluster number until later finalized.

    Args:
        df_cluster (pandas.DataFrame): DataFrame of cluster designations with index of Genome_ names and
            columns of cluster distance levels (0 - 0.99)
        genome (str): Closest related Genome name in DB with finalized cgMLST clusters
        distance (float): cgMLST profile distance to Genome_

    Returns:
        dict: cgMLST cluster assignment profile for new Genome_
    """
    cluster_levels = list(df_cluster.columns)
    df_genome_clusters = df_cluster.ix[genome]
    genome_clusters_dict = {i: x for i, x in df_genome_clusters.iteritems()}
    if distance > 0.0:
        novel_cluster_levels = get_new_cluster_levels(cluster_levels, distance)
        for novel_cluster_level in novel_cluster_levels:
            genome_clusters_dict[novel_cluster_level] = -1
    return genome_clusters_dict



def generate_cgmlst_clusters(df_cluster, genome, distance):
    """
    Get the cgMLST cluster designations to a new Genome_ given a public or private Genome_ in the DB that it matches to most closely and the distance at which the new Genome_ matches.
    New cluster designations at a cluster level will be +1 of the current max for that cluster level.
    If distance is 0.0 then the clusters for the closest matching Genome_ will be returned.

    Args:
        df_cluster (pandas.DataFrame): DataFrame of cluster designations with index of Genome_ names and
            columns of cluster distance levels (0 - 0.99)
        genome (str): Closest related Genome_ name in DB
        distance (float): cgMLST profile distance to Genome_

    Returns:
        dict: cgMLST cluster assignment profile for new Genome_
    """
    cluster_levels = list(df_cluster.columns)
    df_genome_clusters = df_cluster.ix[genome]
    genome_clusters_dict = {i: x for i, x in df_genome_clusters.iteritems()}
    if distance > 0.0:
        novel_cluster_levels = get_new_cluster_levels(cluster_levels, distance)
        for novel_cluster_level in novel_cluster_levels:
            new_cl_num = df_cluster[novel_cluster_level].max() + 1
            genome_clusters_dict[novel_cluster_level] = new_cl_num
    return genome_clusters_dict


@cache.memoize(timeout=int(1e7))
def mist_marker_results(genome_id, test_id):
    """
    Get the list of marker results for a specified Genome_ and MistTest_ ordered by MistMarker_ id.

    Args:
        genome_id (int): Genome_ id
        test_id (int): MistTest_ id

    Returns:
        list: List of marker results as Integers (or None if missing) ordered by MistMarker_ id
    """
    return [int(r) if r is not None else None for r, in session.query(MistMarkerResult.result) \
        .filter(MistMarkerResult.test_id == test_id,
                MistMarkerResult.genome_id == genome_id) \
        .order_by(MistMarkerResult.marker_id).all()]


@cache.memoize(timeout=int(1e7))
def mist_marker_names(test_id):
    """
    Get the names of MistMarker_ belonging to a MistTest_

    Args:
        test_id (int): MistTest_ id

    Returns:
        list: List of MistMarker_ names for a specified MistTest_ ordered by MistMarker_ id
    """
    return [name for id, name in session.query(MistMarker.id, MistMarker.name) \
        .filter(MistMarker.test_id == test_id) \
        .order_by(MistMarker.id) \
        .all()]


@cache.memoize(int(1e7))
def cgmlst_profiles(genome_names, cgmlst_test_name='wgMLST_330'):
    """
    Get the cgMLST profiles for a list of specified Genomes_.
    The returned dictionary contains the results in an array which can be easily transformed into
    a Numpy/Pandas matrix for proportional Hamming distance computation.

    Notes:
        Querying for cgMLST profiles is split into 2 separate queries:
        - query for the id of the cgMLST (wgMLST_330) MIST test
        - using the cgMLST MIST test id and the list of Genome_ names, get all
            of the cgMLST results ordered first by Genome_ id and secondly by
            MistMarker_ id so that results from separate queries can be merged easily

    Args:
        genome_names (list): List of Genome_ names to retrieve cgMLST profiles for
        cgmlst_test_name (str): Name of cgMLST MistTest_ (default=`wgMLST_330`)

    Returns:
        dict: Genome_ names, cgMLST marker names and ordered list of cgMLST allele match results.
    """
    wgmlst_id = session.query(MistTest.id).filter(MistTest.name == cgmlst_test_name).scalar()

    # get genome names ordered by id
    genome_list_query = session.query(Genome.id, Genome.name) \
        .filter(Genome.is_analyzed,
                Genome.name.in_(genome_names)) \
        .order_by(Genome.id)

    ids_names = [(id, name) for id, name in genome_list_query.all()]
    genome_ids = [id for id, name in ids_names]
    genome_names = [name for id, name in ids_names]

    # get marker names ordered by id
    markers_names = mist_marker_names(wgmlst_id)

    # Flask-Cache cache.cache is
    # from werkzeug.contrib.cache import RedisCache
    redis_cache = cache.cache
    #: dict: genome id to ordered list of MIST marker results
    results_dict = defaultdict(list)
    #: str: base Redis key string format
    key_fmt = 'cgmlst-{}'
    #: list: Genome ids that could not be fetched from Redis (non-existent)
    todo_ids = []
    for genome_id in genome_ids:
        v = redis_cache.get(key_fmt.format(genome_id))
        if v is None:
            todo_ids.append(genome_id)
        else:
            results_dict[genome_id] = v
    # query for tuples of genome id and MIST marker result for cgMLST profiles not present in Redis cache
    q = session.query(MistMarkerResult.genome_id,
                      MistMarkerResult.result) \
        .filter(MistMarkerResult.test_id == wgmlst_id,
                MistMarkerResult.genome_id.in_(todo_ids)) \
        .order_by(MistMarkerResult.genome_id,
                  MistMarkerResult.marker_id)
    # execute query q and accumulate results into dict of id to list of results
    # assumed that results are in order
    for gid, r in q.all():
        rint = int(r) if r is not None else None
        results_dict[gid].append(rint)
    # set key-value for each cgMLST profile that needed to be fetched from the PG DB
    for todo_id in todo_ids:
        if todo_id in results_dict:
            # cannot set timeout 0 (indefinite) so setting large timeout instead
            redis_cache.set(key_fmt.format(todo_id),
                            results_dict[todo_id],
                            timeout=int(1e7))
    #: list: all cgMLST results in genome id, marker id order
    results = [x for id in genome_ids for x in results_dict[id]]
    return {'genomes': genome_names, 'markers': markers_names, 'results': results}


def cgmlst_clusters_dataframe():
    """
    Get a Pandas DataFrame of all the cgMLST clusters in the DB.
    Row names are Genome_ names.
    Column names are cgMLST cluster levels (0 - 0.99).

    Returns:
        pandas.DataFrame: All cgMLST clusters in a Pandas DataFrame
    """
    genomes_clusters = {name: cl for name, cl in
                        session.query(Genome.name, Genome.cgmlst_clusters) \
                            .filter(Genome.is_analyzed == True).all()}
    df = pd.DataFrame(genomes_clusters).transpose()
    return df


def get_cluster_level_and_number(df_clusters, genome_clusters):
    """
    Determine the cluster level (0-99%) at which the current genome matches a curated
    and trusted public genome. This method extracts the first level at which the User_
    uploaded Genome_ matches a public Genome_ and the cluster number for that level.

    Args:
        df_clusters (pandas.DataFrame): Table of curated and trusted public genome
            cgMLST clusters where ``index``=genome names and ``columns``=cluster levels
        genome_clusters (list): cgMLST cluster numbers at all cluster levels (0-99%)
            for the User_ uploaded Genome_

    Returns:
        (int, float) | (None, None): Cluster number and level at which the User_
            uploaded matches a public Genome_.
    """
    matching_cluster_level = None
    cluster_number = None
    for cl_level in df_clusters.columns:
        cl_num = genome_clusters[cl_level]
        if df_clusters[df_clusters[cl_level] == cl_num].shape[0] > 0:
            matching_cluster_level = cl_level
            cluster_number = genome_clusters[cl_level]
            break
    return cluster_number, matching_cluster_level