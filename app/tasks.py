'''
Celery tasks
============

Celery is used to execute various tasks ranging from writing updated allele FASTA files to
running analysis pipelines for user-uploaded genomes.
'''
from __future__ import absolute_import

from datetime import datetime
from subprocess import PIPE, Popen
from cStringIO import StringIO
import shutil
from celery.exceptions import Ignore
import os
import pandas as pd
import numpy as np
from Bio import SeqIO
from celery import chain, Celery

from . import session, logger
from .models import Genome, Contig, User, MistTest, MistAllele, UserRole, MistMarkerResult, MistMetadataResult, MistMarker
from .db_loaders import DefaultDataLoader
from .parsers import MistJSONParser
from .cgmlst import find_closest_related_genome, generate_cgmlst_clusters, cgmlst_clusters_dataframe, cgmlst_profiles, generate_temp_cgmlst_clusters
from .blast_wrapper import BlastRunner
from redis import Redis
from .serovar_prediction import SerovarPredictor, get_mlst_serovar_prediction, determine_serovar_from_cgmlst_and_antigen_blast


#: set: valid IUPAC nucleotide characters for checking FASTA format
VALID_NUCLEOTIDES = {'A', 'a',
                     'C', 'c',
                     'G', 'g',
                     'T', 't',
                     'R', 'r',
                     'Y', 'y',
                     'S', 's',
                     'W', 'w',
                     'K', 'k',
                     'M', 'm',
                     'B', 'b',
                     'D', 'd',
                     'H', 'h',
                     'V', 'v',
                     'N', 'n',
                     'X', 'x', } # X for masked nucleotides



def analyse_genome(app, fasta_temp_file, user_name, genome_name, run_rmlst):
    """
    Analyse a user uploaded genome
    ==============================

    Start a Celery chained task that does the following:

     - check that FASTA file string is valid FASTA format
     - add Genome_ and contigs to DB
     - create a temp analysis folder for the Genome_ and User_
     - write a temp FASTA file to the temp analysis folder
     - run QUAST_
     - parse QUAST_ quality stats into DB
     - run MIST_
     - from MIST_ output, parse all MistMetadataResult_ and MistMarkerResult_ into DB
     - determine cgMLST cluster membership for Genome_
     - determine SerovarPrediction_ using antigen BLAST search and comparison to cgMLST clusters
     - clean up temp analysis folder

    Notes:
        A Celery task chain object is returned so that it can be iterated to show the current
        status of the tasks within the chain and if any subtasks have failed.

    Args:
        fasta_str (str): User uploaded genome FASTA file contents string
        user_name (str): User name
        genome_name (str): Genome name
        run_rmlst (bool): Should rMLST MIST test be run?

    Returns:
        object: Chain task object
    """
    temp_dir = app.config['TEMP_DIR']
    user_temp_dir = os.path.join(temp_dir, 'SISTR', '{0}-{1}'.format(user_name, genome_name))
    quast_bin_path = app.config['QUAST_BIN_PATH']

    mist_bin = app.config['MIST_BIN_PATH']
    mist_alleles_dir = app.config['MIST_ALLELES_DIR']
    mist_markers_files = app.config['MIST_MARKERS_FILES']

    public_username = app.config['PUBLIC_SISTR_USER_NAME']

    task = genome_analysis_task.delay(public_username,
                               user_name,
                               genome_name,
                               fasta_temp_file,
                               user_temp_dir,
                               mist_bin,
                               quast_bin_path,
                               mist_alleles_dir,
                               mist_markers_files,
                               run_rmlst)

    return task


def make_celery(flask_app=None, config_name='prod'):
    from . import create_app

    flask_app = flask_app or create_app(config_name)
    c = Celery(flask_app.import_name)
    c.app = flask_app
    logger.info('celery {} flask app {}'.format(c, c.app))
    c.config_from_object('celeryconf')
    TaskBase = c.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            logger.error('ContextTask {}'.format(flask_app))
            with flask_app.app_context():
                logger.error('ContextTask app context {}'.format(flask_app))
                return TaskBase.__call__(self, *args, **kwargs)

    c.Task = ContextTask
    return c


CELERY = make_celery()

from datetime import datetime
import os


@CELERY.task(bind=True)
def genome_analysis_task(self,
                  public_username,
                  user_name,
                  genome_name,
                  fasta_temp_file,
                  user_temp_dir,
                  mist_bin,
                  quast_bin_path,
                  mist_alleles_dir,
                  mist_markers_files,
                  run_rmlst,):

    with open(fasta_temp_file) as f:
        fasta_str = f.read()
    os.remove(fasta_temp_file)

    time_start = datetime.now()



    try:
        self.update_state(state='PROGRESS',
                      meta={'progress':0,
                            'desc': "FASTA format check",})
        fasta_format_check(user_name, genome_name, fasta_str)

        self.update_state(state='PROGRESS',
                          meta={'progress':3,
                                'desc': "Add genome to database",})
        add_genome_to_db(user_name, genome_name, fasta_str)
        self.update_state(state='PROGRESS',
                          meta={'progress':6,
                                'desc': "Create temp genome analysis directory",})
        create_temp_genome_analysis_directory(genome_name, user_name, user_temp_dir)
        self.update_state(state='PROGRESS',
                          meta={'progress':9,
                                'desc': "Write temp genome FASTA",})
        fasta_path = write_temp_genome_fasta(user_temp_dir, genome_name, fasta_str)
        self.update_state(state='PROGRESS',
                          meta={'progress':12,
                                'desc': "Run QUAST ",
                                })

        quast_out_path = run_quast(fasta_path, user_name, genome_name, quast_bin_path)

        self.update_state(state='PROGRESS',
                          meta={'progress':15,
                                'desc': "Parse QUAST results",
                                })
        parse_quast_results(quast_out_path, user_name, genome_name)
        self.update_state(state='PROGRESS',
                          meta={'progress':33,
                                'desc': "Run MIST in silico typing assays",
                                })
        mist_out_path = run_mist(genome_name, user_name, user_temp_dir, mist_bin, mist_alleles_dir, mist_markers_files, run_rmlst)
        self.update_state(state='PROGRESS',
                          meta={'progress':66,
                                'desc': "Parse MIST in silico typing results",
                                })
        parse_mist_results(mist_out_path, user_name)

        if run_rmlst:
            self.update_state(state='PROGRESS',
                          meta={'progress':70,
                                'desc': "Interpreting rMLST results" if run_rmlst else None,
                                })
            interpret_rmlst(mist_out_path, genome_name, mist_markers_files)
        self.update_state(state='PROGRESS',
                          meta={'progress':75,
                                'desc': "Assign cgMLST clusters",
                                })
        assign_cgmlst_clusters(mist_out_path, user_name, genome_name, public_username)
        self.update_state(state='PROGRESS',
                          meta={'progress':85,
                                'desc': "Refine serovar prediction",
                                })
        refine_serovar_prediction(mist_out_path, user_name, genome_name, public_username)
        self.update_state(state='PROGRESS',
                          meta={'progress':95,
                                'desc': "Cleanup temp analysis files",})
        set_genome_analyzed(user_name, genome_name)

    except Exception as ex:
        template = "An exception of type {0} occured. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print message
        self.update_state(state='FAILURE',
                          meta={'progress': 100,
                                'desc': ex.message})
        raise Ignore()

    finally:
        try:
            cleanup_temp_dir(temp_dir=user_temp_dir)
        except:
            pass

    elapsed = datetime.now() - time_start

    try:
        r = Redis()
        key_name = 'genome_analysis_task_timings'
        r.rpush(key_name, elapsed.total_seconds())
    except:
        pass


    return True


@CELERY.task()
def send_email(recipients, subject, body):
    from flask_mail import Message
    from . import mail
    print 'Sending email to {} with subject {}'.format(recipients, subject)
    with CELERY.app.app_context():
        with mail.connect() as conn:
            msg = Message(subject=subject,
                          body=body,
                          recipients=recipients)
            conn.send(msg)


@CELERY.task()
def write_mist_alleles():
    """
    Write updated cgMLST alleles
    ============================

    Write updated cgMLST alleles to ``mist_assays/alleles``.
    First write the alleles to a temporary directory then ``shutil.copy`` the FASTA files
    to the ``mist_assays/alleles`` directory.

    Todo:
        Check if it's even necessary to write the new allele FASTA files based on timestamp.
    """
    allele_dir = 'mist_assays/alleles-tmp'
    try:
        os.mkdir(allele_dir)
    except:
        if not os.path.exists(allele_dir):
            raise IOError('Temp cgMLST alleles dir could not be created at {}'.format(allele_dir))
        pass

    t = session \
        .query(MistTest) \
        .filter(MistTest.name == 'wgMLST_330') \
        .first()

    fasta_paths = []
    for m in t.markers.all():
        fasta_path = os.path.join(allele_dir, m.name + '.fasta')
        fasta_paths.append(fasta_path)
        with open(fasta_path, 'w') as fout:
            for a in m.alleles.order_by(MistAllele.timestamp).all():
                fout.write('>{}\n{}\n'.format(a.name, a.seq))

    for fasta_path in fasta_paths:
        shutil.copy(fasta_path, 'mist_assays/alleles/')
    shutil.rmtree(allele_dir)


@CELERY.task()
def delete_old_tmp_users():
    """
    Delete temporary User_ accounts and associated Genome_ and analysis data for temporary
    User_ accounts that are over 2 weeks old.

    Todo:
        Write to logger.info rather than stdout
    """
    dt_2weeks_ago = datetime.fromordinal(datetime.now().toordinal() - 14)
    users = session.query(User).filter(User.role == UserRole.temporary,
                                       User.last_seen <= dt_2weeks_ago).all()
    print 'Deleting temporary {} Users last seen on or before {}'.format(
        len(users),
        dt_2weeks_ago)
    for user in users:
        print 'Deleting {} Genomes from User {}'.format(user.genomes.count(), user)
        for g in user.genomes.all():
            print 'Deleting Genome {}'.format(g)
            session.delete(g)
            session.commit()
        session.delete(user)
        session.commit()


def fasta_format_check(user_name, genome_name, fasta_str):
    """
    FASTA format check
    ==================

    Check that a FASTA file contents string is valid FASTA format.

     - First non-blank line needs to begin with a '>' header character.
     - Sequence can only contain valid IUPAC nucleotide characters

    Args:
        fasta_str (str): FASTA file contents string
        user_name (str): User name
        genome_name (str): Genome name

    Returns:
        str: FASTA file contents string if valid FASTA format

    Raises:
        Exception: If invalid FASTA format
    """
    sio = StringIO(fasta_str)
    header_count = 0
    line_count = 1
    nt_count = 0
    for l in sio.readlines():
        l = l.strip()
        if l == '':
            continue
        if l[0] == '>':
            header_count += 1
            continue
        if header_count == 0 and l[0] != '>':
            error_msg = 'User="{user}";Genome="{genome}": first non-blank line (L:{line_count}) does not contain FASTA header. Line beginning with ">" expected.' \
                .format(line_count=line_count,
                        genome=genome_name,
                        user=user_name)
            logger.error(error_msg)
            raise Exception(error_msg)
        non_nucleotide_chars_in_line = set(l) - VALID_NUCLEOTIDES

        if len(non_nucleotide_chars_in_line) > 0:
            error_msg = 'User="{user}";Genome="{genome}": line {line} contains the following non-nucleotide characters: {non_nt_chars}' \
                .format(genome=genome_name,
                        user=user_name, line=line_count,
                        non_nt_chars=non_nucleotide_chars_in_line)
            logger.error(error_msg)
            raise Exception(error_msg)
        nt_count += len(l)
        line_count += 1

    if nt_count == 0:
        error_msg = 'User="{user}";Genome="{genome}": FASTA string does not contain any nucleotide sequence.'.format(
            genome=genome_name,
            user=user_name)
        logger.error(error_msg)
        raise Exception(error_msg)

    logger.info('User="{user}";Genome="{genome}": valid FASTA format (len={length})'.format(
        user=user_name,
        genome=genome_name,
        length=len(fasta_str)))
    return user_name


def add_genome_to_db(user_name, genome_name, fasta_str):
    """
    Add Genome_ and all Contig_ to DB
    =================================

    Add User_ Genome_ and all Contig_ for Genome_ to database.

    Notes:
        BioPython FASTA parser used to parse FASTA string.

    Args:
        fasta_str (str): Genome FASTA file contents
        user_name (str): User name
        genome_name (str): A unique Genome name

    Returns:
        str: Name of genome added to the database

    Raises:
        Exception: if Genome_ or Genome_ Contig_ cannot be added to the DB
    """
    genome_name = Genome.make_unique_name(genome_name, session)
    user = session.query(User).filter(User.name == user_name).first()
    assert user is not None, 'User {0} not found in DB'.format(user_name)
    genome = Genome(name=genome_name,
                    user=user,
                    time_uploaded=datetime.utcnow())

    session.add(genome)
    try:
        session.commit()
        logger.info('Added user {0} genome {1} to DB'.format(user_name, genome_name))
    except:
        session.rollback()
        error_msg = 'Could not add genome {0} of user {1} to DB. Rolling back DB transaction'.format(
            genome_name,
            user_name)
        logger.error(error_msg)
        raise Exception(error_msg)

    contigs = [Contig(name=rec.description,
                      seq=str(rec.seq),
                      genome=genome)
               for rec in SeqIO.parse(StringIO(fasta_str), 'fasta')]

    session.add_all(contigs)
    try:
        session.commit()
        logger.info('Added {0} contigs for genome {1} of user {2} to DB.'.format(
            len(contigs),
            genome_name,
            user_name
        ))
    except:
        session.rollback()
        error_msg = 'Could not add contigs for genome {0} of user {1} to DB. Rolling back DB transaction.'.format(
            genome_name,
            user_name)
        logger.error(error_msg)
        raise Exception(error_msg)
    return genome_name


def create_temp_genome_analysis_directory(genome_name, user_name, user_temp_dir):
    """
    Create temp genome analysis directory
    =====================================

    Create a temporary genome analysis directory for analysis of the user uploaded genome.

    Args:
        genome_name (str): Genome name
        user_name (str): User name
        user_temp_dir (str): Base temporary genome analysis directory

    Returns:
        str: Temporary genome analysis directory

    Raises:
        IOError: if temp analysis directory cannot be created
    """
    user = session.query(User).filter(User.name == user_name).first()
    assert user is not None, 'User {0} not found in DB'.format(user_name)
    try:
        os.makedirs(user_temp_dir)
    except:
        # TODO: if analysis dir cannot be made then retry a little while later or in a different location
        if not os.path.exists(user_temp_dir):
            raise IOError('Temp genome analysis directory cannot be created for User={} and Genome={}.'.format(
                user_name,
                genome_name
            ))
        pass
    return user_temp_dir


def write_temp_genome_fasta(dir_path, genome_name, fasta_str):
    """
    Write temp Genome_ FASTA file
    =============================

    Write a temporary FASTA format file of a Genome_ to a User_ specific analysis directory.

    Args:
        dir_path (str): Temp user analysis directory to write FASTA file to
        genome_name (str): User uploaded genome name
        fasta_str (str): FASTA file contents

    Returns:
        str: File path to the temporary FASTA file that was written
    """
    temp_genome_fasta_path = os.path.join(dir_path, genome_name + '.fasta')
    with open(temp_genome_fasta_path, 'w') as f:
        f.write(fasta_str)
    return temp_genome_fasta_path


def run_quast(fasta_filepath, user_name, genome_name, quast_bin_path):
    """
    Run QUAST_
    ==========

    Run QUAST_ to generate assembly quality stats for a User_ uploaded Genome_.

    Args:
        fasta_filepath (str): Genome FASTA file path
        user_name (str): User name
        genome_name (str): User uploaded genome name

    Returns:
        str: File path of QUAST_ ``transposed_report.tsv``

    Raises:
        Exception: if QUAST_ ``transposed_report.tsv`` does not exist (i.e. not created)
    """
    temp_output_dir = os.path.dirname(fasta_filepath)
    quast_output_dir = os.path.join(temp_output_dir, 'quast-' + genome_name)
    p = Popen([quast_bin_path,
               '--no-plot', # disable plotting since it's unneeded
               '-f', # gene finding
               '-o', os.path.abspath(quast_output_dir),
               os.path.abspath(fasta_filepath)],
              stdout=PIPE,
              stderr=PIPE)
    p.wait()

    quast_report_path = os.path.join(quast_output_dir, 'transposed_report.tsv')

    if os.path.exists(quast_report_path):
        return quast_report_path
    else:
        raise Exception('QUAST report not generated for genome {0} for user {1}'.format(
            genome_name,
            user_name,))


def parse_quast_results(quast_output_path, user_name, genome_name):
    """
    Parse QUAST_ output
    ===================

    Parse the QUAST_ assembly stats for a user uploaded Genome_ into the DB.

    Args:
        quast_output_path (str): QUAST output file path
        user_name (str): User name
        genome_name (str): Genome name

    Returns:
        str: Genome name
    """
    ddl = DefaultDataLoader(session)
    ddl.load_assembly_stats(quast_output_path)
    genome = session.query(Genome) \
        .join(Genome.user) \
        .filter(User.name == user_name) \
        .filter(Genome.name == genome_name).first()
    assert genome is not None, 'Genome {0} of user {1} does not exist in the DB'.format(
        genome_name,
        user_name
    )
    assert genome.quality_stats is not None, 'Genome {0} of user {1} does not have QUAST generated quality stats'.format(
        genome_name,
        user_name
    )
    return genome_name


def run_mist(genome_name, user_name, temp_output_dir, mist_bin, mist_alleles_dir, mist_markers_files, run_rmlst):
    """
    Run MIST
    ========

    Run MIST on the Genome_ with the following *in silico* assays:

    - MLST
    - wgMLST_330 (cgMLST with 330 markers)

    Args:
        fasta_filepath (str): File path to temporary FASTA file of User_ uploaded Genome_
        temp_output_dir (str): Temporary analysis directory
        user_name (str): User_ name
        genome_name (str): Genome_ name
        mist_bin (str): Path to MIST.exe
        mist_alleles_dir (str): Path to MIST sequence-typing alelles directory
        mist_markers_files (dict): Dict of MIST test names to MIST markers file paths
        run_rmlst (bool): Run rMLST typing test?

    Returns:
        str: MIST JSON output file path

    Raises:
        Exception: if MIST JSON output file does not exist
    """
    temp_genome_fasta_path = os.path.join(temp_output_dir, genome_name + '.fasta')
    mist_temp_dir = os.path.join(temp_output_dir, 'mist_tmp-{0}'.format(genome_name))
    mist_output_filepath = os.path.join(temp_output_dir, 'MIST-{0}.json'.format(genome_name))
    # run MIST on all assays
    args = ['mono',
               os.path.abspath(mist_bin),
               '-c', '1',
               '-T', mist_temp_dir,
               '-j', mist_output_filepath,
               '-a', os.path.abspath(mist_alleles_dir),
               '-t', os.path.abspath(mist_markers_files['MLST']),
               '-t', os.path.abspath(mist_markers_files['wgMLST_330']),
               '-t' if run_rmlst else None,
               os.path.abspath(mist_markers_files['rMLST']) if run_rmlst else None,
               temp_genome_fasta_path,]
    args = filter(None, args)
    p = Popen(args)
    p.wait()
    assert os.path.exists(mist_output_filepath), 'MIST output for genome {0} of user {1} does not exist at {2}.'.format(
        genome_name,
        user_name,
        mist_output_filepath
    )
    return mist_output_filepath


def parse_mist_results(mist_output_path, user_name):
    """
    Parse MIST output
    =================

    Parse MistMarkerResult_ and MistMetadataResult_ from MIST_ JSON output.

    Args:
        mist_output_path (str): MIST JSON output file path
        user_name: User name

    Returns:
        str: MIST JSON output file path
    """
    user = session.query(User).filter(User.name == user_name).first()
    assert user is not None, 'User {0} does not exist in the database.'.format(user_name)
    assert os.path.exists(mist_output_path)
    parser = MistJSONParser(session, user, mist_output_path)
    parser.parse_all_marker_results()
    parser.parse_all_test_metadata()
    return mist_output_path


def interpret_rmlst(temp_file_path, genome_name, mist_markers_files):
    """
    Find the closest matching rMLST profile in the rMLST profiles table (e.g. `rMLST.txt`) since MIST only
    does perfect matching to this table.
    Most genome rMLST profiles should have at least a partial match to some profile.
    The percent of matching alleles is also reported as a decimal.

    Args:
        temp_file_path (str): Temporary file path argument passed to next task in chain
        genome_name (str): Genome_ name
        mist_markers_files (dict): MistTest_ name to MIST `markers` file path

    Returns:
        str: `temp_file_path` - Temporary file path argument passed to next task in chain
    """
    rmlst_test_id = session.query(MistTest.id).filter(MistTest.name == 'rMLST').first()

    genome = session.query(Genome).filter(Genome.name == genome_name).first()

    query = session.query(MistMarker.name, MistMarkerResult.result) \
        .join(MistMarkerResult) \
        .filter(MistMarkerResult.test_id == rmlst_test_id,
                MistMarkerResult.genome_id == genome.id)
    rmlst_results = {name: result for name, result in query.all()}

    assert isinstance(genome, Genome)

    rmlst_markers_file = mist_markers_files['rMLST']
    rmlst_table_path = rmlst_markers_file.replace('.markers', '.txt')
    # read rMLST table as string
    df_rmlst = pd.read_table(rmlst_table_path, dtype='string_')
    # remove rows with no genus and species data
    df_rmlst = df_rmlst[~(pd.isnull(df_rmlst['genus']) & pd.isnull(df_rmlst['species']))]
    # determine matching rows for each marker
    matching_series = []
    for marker, result in rmlst_results.iteritems():
        series = df_rmlst[marker] == result
        matching_series.append(series)
    df_matching = pd.DataFrame(matching_series)
    # find the max matching rows
    matching_sums = [np.sum(df_matching[x]) for x in df_matching]
    max_matching = max(matching_sums)
    is_max_matching_bools = pd.Series(matching_sums) == max_matching
    print 'Max matching rMLST alleles is {} for genome {}'.format(max_matching, genome_name)
    non_marker_columns = set(df_rmlst.columns) - set(rmlst_results.keys())
    # need to convert to lists to avoid unalignable Series errors
    df_matching = df_rmlst[list(is_max_matching_bools)][list(non_marker_columns)]
    # get the matching rMLST results
    md = {}
    for c in df_matching:
        col_vals = df_matching[c]
        col_vals = col_vals[~(pd.isnull(col_vals))]
        # delimit unique metadata values with '|'
        s = '|'.join(set(col_vals))
        if s == '':
            continue
        md[c] = s
    # need to store as string to avoid psycopg2.DataError
    md['matching_alleles'] = str(float(max_matching / float(len(rmlst_results))))
    # update rMLST metadata for genome
    rmlst_md_result = session.query(MistMetadataResult) \
        .filter(MistMetadataResult.genome_id == genome.id,
                MistMetadataResult.test_id == rmlst_test_id).first()
    assert isinstance(rmlst_md_result, MistMetadataResult)
    # update the rMLST test results
    rmlst_md_result.attrs = md
    session.add(rmlst_md_result)
    session.commit()

    return temp_file_path


def assign_cgmlst_clusters(temp_file_path, user_name, genome_name, public_username):
    """
    Determine cgMLST cluster membership
    ===================================

    Find the closest related fully analysed public or private genome to the user
    uploaded genome and assign clusters to the user genome based off the public
    genome up to the level of distance that is shared between the user uploaded genome
    and the closest related genome in the DB.
    Cluster levels at which novel cluster numbers need to be assigned are assigned the max
    allele count +1.

    Notes:
        Curated and trusted public Genome_ cgMLST_ clusters are used in cgMLST-based serovar prediction.
        All Genomes_ are used in

    Args:
        temp_file_path (str): Temporary genome FASTA file path
        user_name (str): User_ name
        genome_name (str): Genome_ name
        public_username (str): Public SISTR username

    Returns:
        str: Temporary genome FASTA file path
    """
    user = session.query(User).filter(User.name == user_name).first()
    public_user = session.query(User).filter(User.name == public_username).first()
    df = cgmlst_clusters_dataframe()

    genome = session.query(Genome) \
        .filter(Genome.user_id == user.id,
                Genome.name == genome_name) \
        .first()

    public_genomes = [name for name, in session.query(Genome.name) \
        .filter(Genome.user_id == public_user.id,
                Genome.is_analyzed,
                Genome.is_cgmlst_clusters_final) \
        .order_by(Genome.id) \
        .all()]

    #: Public Genomes_ are cached so they can be retrieved and checked very quickly
    with CELERY.app.app_context():
        public_profiles = cgmlst_profiles(public_genomes)
    closest_genome, other_related_genomes = find_closest_related_genome(genome_name, public_profiles)
    public_distance = closest_genome['distance']
    public_genome_name = closest_genome['genome']
    #: Check if any public genomes match cgMLST profile for User_ Genome_ perfectly and if so then skip checking private Genomes_
    if public_distance == 0.0:
        public_genome = session.query(Genome).filter(Genome.name == public_genome_name).first()
        assert isinstance(public_genome, Genome)
        genome.cgmlst_clusters = public_genome.cgmlst_clusters
        genome.is_cgmlst_clusters_final = True
        session.add(genome)
        session.commit()
        return temp_file_path

    #: Determine closest matching private Genome_ to User_ Genome_
    analyzed_genomes = [name for name, in session.query(Genome.name) \
        .filter(Genome.user_id != public_user.id,
                Genome.is_analyzed,
                Genome.is_cgmlst_clusters_final) \
        .order_by(Genome.id) \
        .all()]
    with CELERY.app.app_context():
        analyzed_genome_profiles = cgmlst_profiles(analyzed_genomes)
    closest_genome, other_related_genomes = find_closest_related_genome(genome_name, analyzed_genome_profiles)
    analyzed_distance = closest_genome['distance']
    analyzed_genome_name = closest_genome['genome']

    closest_genome_name = public_genome_name
    closest_distance = public_distance
    #: Determine if public or private Genome_ matches more closely
    if analyzed_distance < public_distance:
        closest_genome_name = analyzed_genome_name
        closest_distance = analyzed_distance

    #: Assign cgMLST clusters
    if analyzed_distance == 0.0:
        #: if cgMLST profile match is 100% then assign cgMLST clusters as is
        perf_match_genome = session.query(Genome).filter(Genome.name == closest_genome_name).first()
        assert isinstance(perf_match_genome, Genome)
        genome.cgmlst_clusters = perf_match_genome.cgmlst_clusters
        genome.is_cgmlst_clusters_final = True
    else:
        genome.cgmlst_clusters = generate_temp_cgmlst_clusters(df, closest_genome_name, closest_distance)
        genome.is_cgmlst_clusters_final = False
    session.add(genome)
    session.commit()
    return temp_file_path


def refine_serovar_prediction(temp_file_path, user_name, genome_name, public_username):
    """
    Determine the serovar prediction based on antigen BLAST data (and SGSA if needed) in combination with
    cgMLST cluster membership with public SISTR genomes.

    - Initialize a BlastRunner object with the temporary working directory and FASTA file path
    - Predict H1, H2 and serogroup antigens using curated fliC, fljB and wzx/wzy FASTA files
    - Predict antigen BLAST based serovar prediction (may be more than one matching serovar)
    - Determine cgMLST based serovar prediction based on curated public genomes and cluster membership analysis
    - Determine final serovar call based on combination of antigen-based and cgMLST cluster-based serovar prediction
    - Get MLST serovar prediction from MLST ST (may be of interest to the user; does not affect final serovar call)

    Args:
        temp_file_path (str): Temporary genome FASTA file path
        user_name (str): User name
        genome_name (str): Genome name
        public_username (str): Public SISTR username

    Returns:
        str: Temporary genome FASTA file path
    """
    tmp_dir = os.path.dirname(temp_file_path)
    fasta_path = os.path.join(tmp_dir, '{}.fasta'.format(genome_name))
    blast_runner = BlastRunner(fasta_path, tmp_dir)
    serovar_predictor = SerovarPredictor(blast_runner)
    serovar_predictor.predict_serovar_from_antigen_blast()

    user = session.query(User).filter(User.name == user_name).first()
    genome = session.query(Genome).filter(Genome.user_id == user.id, Genome.name == genome_name).first()

    serovar_pred = serovar_predictor.get_serovar_prediction(session, user, genome)


    determine_serovar_from_cgmlst_and_antigen_blast(public_username, genome, serovar_pred, serovar_predictor, user)

    get_mlst_serovar_prediction(genome, serovar_pred)

    session.add(serovar_pred)
    session.commit()

    return temp_file_path


def set_genome_analyzed(user_name, genome_name):
    """
    Clean up temporary genome analysis data for the user uploaded genome and set Genome_ as analyzed.

    .. code-block:: python

        genome.is_analyzed = True

    Args:
        temp_file_path (str): File path for last file generated in the temporary genome analysis folder
    """
    user = session.query(User).filter(User.name == user_name).first()
    genome = session.query(Genome).filter(Genome.user_id == user.id, Genome.name == genome_name).first()
    genome.is_analyzed = True
    session.add(genome)
    session.commit()


def cleanup_temp_dir(temp_dir):
    shutil.rmtree(temp_dir)