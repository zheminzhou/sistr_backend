from collections import defaultdict
from app import cache, session
from app.models import Genome, MistTest, MistMetadataResult, User, GeographicLocation

__author__ = 'peter'


def listdict_to_dictlist(ld):
    assert isinstance(ld, list)
    ks = set([k for d in ld for k, v in d.iteritems()])
    dl = defaultdict(list)
    for d in ld:
        for k in ks:
            dl[k].append(d[k] if k in d else None)
    return dict(dl)


@cache.memoize(timeout=int(1e7))
def get_genome_metadata_dict(genome_id):
    """
    Get a dictionary of genome metadata for a specified Genome_

    Args:
        genome (int): Genome_ DB id

    Returns:
        dict: Metadata attributes for a Genome_
    """
    genome = session.query(Genome).filter(Genome.id == genome_id).first()
    date_str = None
    date_year = None
    date_month = None
    if genome.collection_date is not None:
        dt = genome.collection_date
        date_year = dt.year
        date_month = dt.month
        date_str = dt.strftime('%Y/%m/%d')

    mlst_test = session.query(MistTest).filter(MistTest.name == 'MLST').scalar()
    mlst_result = genome.mist_metadata_results.filter(MistMetadataResult.test == mlst_test).first()
    mlst_st = ''
    if mlst_result.attrs:
        mlst_st = mlst_result.attrs['ST']

    serovar_prediction = genome.serovar_prediction.first()
    predicted_serovar = ''
    if serovar_prediction:
        predicted_serovar = serovar_prediction.serovar

    geo_loc = genome.geographic_location
    host = genome.host
    cgmlst_clusters = genome.cgmlst_clusters
    curation_info = None if genome.curation_info is None else genome.curation_info.code
    genome_metadata_dict = {
        'genome': genome.name,
        'user_uploader': genome.user.name,
        'subspecies': genome.subspecies.name if genome.subspecies else None,
        'serovar': genome.serovar,
        'serovar_predicted': predicted_serovar,
        'host_common_name': host.common_name if host else None,
        'host_latin_name': host.latin_name if host else None,
        'collection_date': date_str,
        'collection_year': date_year,
        'collection_month': date_month,
        'source_type': genome.source_type,
        'source_info': genome.source_info,
        'source_country': geo_loc.country if geo_loc else None,
        'source_region': geo_loc.region if geo_loc else None,
        'source_lat': float(geo_loc.lat) if geo_loc else None,
        'source_lng': float(geo_loc.lng) if geo_loc else None,
        'cgMLST_cluster_1%': cgmlst_clusters['0.99'],
        'cgMLST_cluster_10%': cgmlst_clusters['0.9'],
        'cgMLST_cluster_70%': cgmlst_clusters['0.3'],
        'cgMLST_cluster_75%': cgmlst_clusters['0.25'],
        'cgMLST_cluster_80%': cgmlst_clusters['0.2'],
        'cgMLST_cluster_85%': cgmlst_clusters['0.15'],
        'cgMLST_cluster_90%': cgmlst_clusters['0.1'],
        'cgMLST_cluster_95%': cgmlst_clusters['0.05'],
        'cgMLST_cluster_99%': cgmlst_clusters['0.01'],
        'cgMLST_cluster_100%': cgmlst_clusters['0'],
        'MLST_ST_in_silico': mlst_st,
        'curation_info': curation_info,
    }
    misc_metadata_ignore = ['Nominatim_geocode_json',
                            'strain',
                            'sub_species_reported',
                            'location',
                            'NGS_read_length',
                            'NGS_total_bases',
                            # 'pfge_primaryenzyme_pattern',
                            # 'pfge_secondaryenzyme_pattern',
                            # 'outbreak',
    ]
    if genome.misc_metadata is not None:
        for k, v in genome.misc_metadata.iteritems():
            if k in misc_metadata_ignore:
                continue
            genome_metadata_dict[k] = v
    return genome_metadata_dict


def user_genomes_metadata(user_name):
    genome_query = session.query(Genome.id) \
        .join(Genome.user) \
        .filter(User.name == user_name, Genome.is_analyzed)
    md = [get_genome_metadata_dict(genome_id) for genome_id, in genome_query.all()]
    return md


@cache.memoize(timeout=int(1e7))
def get_genome_quality_stats(genome_id):
    genome = session.query(Genome).filter(Genome.id == genome_id).first()
    return genome.quality_stats


@cache.memoize(timeout=int(1e7))
def genome_quality_stats_plus_metadata_dict(genome_id):
    d = get_genome_quality_stats(genome_id)
    # add all QUAST assembly stats to dict
    for k, v in d.iteritems():
        d[k] = float(v)
        # add all genome metadata to dict
    genome_md = get_genome_metadata_dict(genome_id)
    for k, v in genome_md.iteritems():
        d[k] = v
    d['Assembly'] = d['genome']
    # add all serovar prediction data to dict
    # serovar_prediction = genome.serovar_prediction.first()
    serovar_prediction_md = get_basic_serovar_prediction_info(genome_id)
    for k, v in serovar_prediction_md.iteritems():
        d[k] = v
    return d


def genome_assembly_stats(user_name):
    genome_query = session.query(Genome.id) \
        .join(Genome.user) \
        .filter(User.name == user_name,
                Genome.is_analyzed == True)

    genome_stats_md = []
    for genome_id, in genome_query.all():
        genome_stats_md.append(genome_quality_stats_plus_metadata_dict(genome_id))
    return genome_stats_md


def get_serovar_count_str(serovar_counts_dict):
    if serovar_counts_dict is None:
        return
    tuples = [(k, v) for k, v in serovar_counts_dict.iteritems()]
    tuples.sort(key=lambda x: x[1], reverse=True)
    total = sum([count for serovar, count in tuples])
    return ', '.join(['{} ({}/{})'.format(serovar, count, total) for serovar, count in tuples])


@cache.memoize(timeout=int(1e7))
def get_basic_serovar_prediction_info(genome_id):
    genome = session.query(Genome) \
        .filter(Genome.id == genome_id) \
        .first()
    serovar_prediction = genome.serovar_prediction.first()

    curated_serovar = genome.serovar
    reported_serovar = 'reported_serovar'
    reported_serovar = genome.misc_metadata[
        reported_serovar] if genome.misc_metadata is not None and reported_serovar in genome.misc_metadata else None
    curation_info = None if genome.curation_info is None else genome.curation_info.code

    mlst_test = session.query(MistTest).filter(MistTest.name == 'MLST').scalar()
    mlst_result = genome.mist_metadata_results.filter(MistMetadataResult.test == mlst_test).first()
    mlst_st = ''
    if mlst_result.attrs:
        mlst_st = mlst_result.attrs['ST']

    return {'H1_prediction': serovar_prediction.h1,
            'H2_prediction': serovar_prediction.h2,
            'Serogroup_prediction': serovar_prediction.serogroup,
            'serovar_prediction': serovar_prediction.serovar,
            'serovar_antigen_prediction': serovar_prediction.serovar_antigen,
            'cgMLST_cluster_level': serovar_prediction.cgmlst_cluster_level,
            'cgMLST_cluster_number': serovar_prediction.cgmlst_cluster_number,
            'cgMLST_serovar_prediction': serovar_prediction.serovar_cgmlst,
            'cgMLST_serovar_count_predictions': get_serovar_count_str(serovar_prediction.cgmlst_serovar_counts),
            'MLST_ST_in_silico': mlst_st,
            'MLST_serovar_prediction': serovar_prediction.serovar_mlst,
            'MLST_serovar_count_predictions': get_serovar_count_str(serovar_prediction.mlst_serovar_counts),
            'reported_serovar': reported_serovar,
            'curation_info': curation_info,
            'curated_serovar': curated_serovar,
            'genome': genome.name,
            'user_uploader': genome.user.name,
    }


def genome_locations(user_name):
    query_stmt = session.query(Genome.name,
                               GeographicLocation.country,
                               GeographicLocation.region,
                               GeographicLocation.lat,
                               GeographicLocation.lng) \
        .join(GeographicLocation) \
        .join(User) \
        .filter(User.name == user_name, Genome.is_analyzed)
    return [{"genome": name,
             "country": country,
             "region": region,
             "lat": float(lat) if lat is not None else None,
             "lng": float(lng) if lng is not None else None,
            } for name, country, region, lat, lng in query_stmt.all()]
