from collections import Counter
from app.blast_wrapper import BlastReader
from app.cgmlst import public_genomes_clusters_table, get_cluster_level_and_number

__author__ = 'piotr'

from app import logger, session
import pandas as pd

from app.models import \
    WzxPrediction, \
    WzyPrediction, \
    SerogroupPrediction, \
    H1FliCPrediction, \
    H2FljBPrediction, SerovarPrediction, MistTest, MistMetadataResult, User, Genome, CurationInfo

from app.serovar_prediction.constants import \
    FLJB_FASTA_PATH, \
    FLIC_FASTA_PATH, \
    H2_FLJB_SIMILARITY_GROUPS, \
    H1_FLIC_SIMILARITY_GROUPS, \
    WZY_FASTA_PATH, \
    WZX_FASTA_PATH, \
    SEROGROUP_SIMILARITY_GROUPS, \
    SEROVAR_TABLE_PATH, MLST_TO_SEROVAR_MATRIX_PATH


def get_antigen_name(qseqid):
    """
    Get the antigen name from the BLASTN result query ID.
    The last item delimited by | characters is the antigen name for all
    antigens (H1, H2, serogroup)

    @type qseqid: str
    @param qseqid: BLASTN result query ID
    @return: antigen name
    """
    if qseqid:
        return qseqid.split('|')[-1]


def serovar_table():
    """
    Get the WHO 2007 Salmonella enterica serovar table with serogroup, H1 and
    H2 antigen info as a Pandas DataFrame.
    @return: Pandas DataFrame of serovar table
    """
    return pd.read_csv(SEROVAR_TABLE_PATH)


class BlastAntigenGeneMixin:
    def get_antigen_gene_blast_results(self, model_obj, antigen_gene_fasta):
        blast_outfile = self.blast_runner.blast_against_query(antigen_gene_fasta)
        blast_reader = BlastReader(blast_outfile)
        is_missing = blast_reader.is_missing
        model_obj.is_missing = is_missing
        if not is_missing:
            model_obj.blast_results = blast_reader.df_dict()

            model_obj.top_result = blast_reader.top_result()
            model_obj.is_perfect_match = blast_reader.is_perfect_match
            model_obj.is_trunc = blast_reader.is_trunc

        return model_obj


class SerogroupPredictor(BlastAntigenGeneMixin):
    def __init__(self, blast_runner):
        """
        SerogroupPredictor takes a initialized BlastRunner object where the
        temp work folder has been created and the genome fasta has been copied
        over and a BLASTN DB has been made for it.
        This class then queries wzx and wzy against the genome using the
        BlastRunner to get the wzx and wzy serogroup predictions.

        @type blast_runner: app.blast_wrapper.BlastRunner
        @param blast_runner: Initialized BlastRunner object
        """
        self.blast_runner = blast_runner

        self.wzx_prediction = WzxPrediction()
        self.wzy_prediction = WzyPrediction()
        self.serogroup_prediction = SerogroupPrediction()


    def search_for_wzx(self):
        self.wzx_prediction = self.get_antigen_gene_blast_results(self.wzx_prediction, WZX_FASTA_PATH)
        if not self.wzx_prediction.is_missing:
            top_result = self.wzx_prediction.top_result
            top_result_pident = top_result['pident']
            top_result_length = top_result['length']

            if top_result_pident < 88.0:
                self.wzx_prediction.is_missing = True
                self.wzx_prediction.serogroup = None
                return

            if top_result_length < 400:
                self.wzx_prediction.is_missing = True
                self.wzx_prediction.serogroup = None
                return

            self.wzx_prediction.serogroup = get_antigen_name(top_result['qseqid'])


    def search_for_wzy(self):
        self.wzy_prediction = self.get_antigen_gene_blast_results(self.wzy_prediction, WZY_FASTA_PATH)
        if not self.wzy_prediction.is_missing:
            top_result = self.wzy_prediction.top_result
            top_result_pident = top_result['pident']
            top_result_length = top_result['length']

            if top_result_pident < 88.0:
                self.wzy_prediction.is_missing = True
                self.wzy_prediction.serogroup = None
                return

            if top_result_length < 600:
                self.wzy_prediction.is_missing = True
                self.wzy_prediction.serogroup = None
                return

            self.wzy_prediction.serogroup = get_antigen_name(top_result['qseqid'])


    def predict(self):
        self.search_for_wzx()
        self.search_for_wzy()
        self.serogroup_prediction.wzx_prediction = self.wzx_prediction
        self.serogroup_prediction.wzy_prediction = self.wzy_prediction
        if self.wzx_prediction.is_perfect_match:
            self.serogroup_prediction.serogroup = self.wzx_prediction.serogroup
        if self.wzy_prediction.is_perfect_match:
            self.serogroup_prediction.serogroup = self.wzy_prediction.serogroup
        if self.wzy_prediction.is_perfect_match or self.wzx_prediction.is_perfect_match:
            return

        if self.wzx_prediction.is_missing and self.wzy_prediction.is_missing:
            return
        if self.wzx_prediction.is_missing:
            self.serogroup_prediction.serogroup = self.wzy_prediction.serogroup
            return
        if self.wzy_prediction.is_missing:
            self.serogroup_prediction.serogroup = self.wzx_prediction.serogroup
            return

        if self.wzy_prediction.serogroup == self.wzx_prediction.serogroup:
            self.serogroup_prediction.serogroup = self.wzx_prediction.serogroup
            return

        top_wzy_result = self.wzy_prediction.top_result
        top_wzx_result = self.wzx_prediction.top_result

        wzx_cov = top_wzx_result['coverage']
        wzx_pident = top_wzx_result['pident']

        wzy_cov = top_wzy_result['coverage']
        wzy_pident = top_wzy_result['pident']

        if wzx_cov >= wzy_cov and wzx_pident >= wzy_pident:
            self.serogroup_prediction.serogroup = self.wzx_prediction.serogroup
        elif wzx_cov < wzy_cov and wzx_pident < wzy_pident:
            self.serogroup_prediction.serogroup = self.wzy_prediction.serogroup
        else:
            self.serogroup_prediction.serogroup = self.wzx_prediction.serogroup


class H1Predictor(BlastAntigenGeneMixin):
    def __init__(self, blast_runner):
        self.blast_runner = blast_runner
        self.h1_prediction = H1FliCPrediction()

    def predict(self):
        self.h1_prediction = self.get_antigen_gene_blast_results(self.h1_prediction, FLIC_FASTA_PATH)
        if not self.h1_prediction.is_missing:
            if not self.h1_prediction.is_perfect_match:
                df_blast_results = pd.DataFrame(self.h1_prediction.blast_results)
                df_blast_results = df_blast_results[
                    (df_blast_results['mismatch'] <= 25) & (df_blast_results['length'] >= 700)]

                if df_blast_results.shape[0] == 0:
                    self.h1_prediction.is_missing = True
                    self.h1_prediction.top_result = None
                    self.h1_prediction.h1 = None
                    return

                df_blast_results_over1000 = df_blast_results[
                    (df_blast_results['mismatch'] <= 5) & (df_blast_results['length'] >= 1000)]

                if df_blast_results_over1000.shape[0] > 0:
                    df_blast_results = df_blast_results_over1000.sort('mismatch')
                else:
                    df_blast_results = df_blast_results.sort('bitscore', ascending=False)

                result_dict = BlastReader.df_first_row_to_dict(df_blast_results)
                result_trunc = BlastReader.is_blast_result_trunc(qstart=result_dict['qstart'],
                                                                 qend=result_dict['qend'],
                                                                 sstart=result_dict['sstart'],
                                                                 send=result_dict['send'],
                                                                 qlen=result_dict['qlen'],
                                                                 slen=result_dict['slen'])
                self.h1_prediction.top_result = result_dict
                self.h1_prediction.is_trunc = result_trunc
            self.h1_prediction.h1 = get_antigen_name(self.h1_prediction.top_result['qseqid'])


class H2Predictor(BlastAntigenGeneMixin):
    def __init__(self, blast_runner):
        self.blast_runner = blast_runner
        self.h2_prediction = H2FljBPrediction()

    def predict(self):
        self.h2_prediction = self.get_antigen_gene_blast_results(self.h2_prediction, FLJB_FASTA_PATH)
        if not self.h2_prediction.is_missing:
            if not self.h2_prediction.is_perfect_match:
                top_result = self.h2_prediction.top_result
                match_len = top_result['length']
                pident = top_result['pident']

                # short lower %ID matches are treated as missing or '-' for H2
                if match_len <= 600 and pident < 88.0:
                    self.h2_prediction.h2 = '-'
                    self.h2_prediction.is_missing = True
                    return

                if match_len <= 600 and not self.h2_prediction.is_trunc:
                    self.h2_prediction.h2 = '-'
                    self.h2_prediction.is_missing = True
                    return

                df_blast_results = pd.DataFrame(self.h2_prediction.blast_results)
                df_blast_results = df_blast_results[
                    (df_blast_results['mismatch'] <= 50) & (df_blast_results['length'] >= 700)]

                if df_blast_results.shape[0] == 0:
                    self.h2_prediction.is_missing = True
                    self.h2_prediction.top_result = None
                    self.h2_prediction.h2 = '-'
                    return

                df_blast_results_over1000 = df_blast_results[
                    (df_blast_results['mismatch'] <= 5) & (df_blast_results['length'] >= 1000)]

                if df_blast_results_over1000.shape[0] > 0:
                    df_blast_results = df_blast_results_over1000.sort('mismatch')
                else:
                    df_blast_results = df_blast_results.sort('bitscore', ascending=False)

                result_dict = BlastReader.df_first_row_to_dict(df_blast_results)
                result_trunc = BlastReader.is_blast_result_trunc(qstart=result_dict['qstart'],
                                                                 qend=result_dict['qend'],
                                                                 sstart=result_dict['sstart'],
                                                                 send=result_dict['send'],
                                                                 qlen=result_dict['qlen'],
                                                                 slen=result_dict['slen'])
                self.h2_prediction.top_result = result_dict
                self.h2_prediction.is_trunc = result_trunc
            self.h2_prediction.h2 = get_antigen_name(self.h2_prediction.top_result['qseqid'])

        if self.h2_prediction.is_missing:
            self.h2_prediction.h2 = '-'


class SerovarPredictor:
    serogroup = None
    h1 = None
    h2 = None
    serovar = None

    def __init__(self, blast_runner):
        """

        """
        self.blast_runner = blast_runner
        self.serogroup_predictor = SerogroupPredictor(self.blast_runner)
        self.h1_predictor = H1Predictor(self.blast_runner)
        self.h2_predictor = H2Predictor(self.blast_runner)

    def predict_antigens(self):
        self.h1_predictor.predict()
        self.h2_predictor.predict()
        self.serogroup_predictor.predict()
        self.h1 = self.h1_predictor.h1_prediction.h1
        self.h2 = self.h2_predictor.h2_prediction.h2
        self.serogroup = self.serogroup_predictor.serogroup_prediction.serogroup
        return self.serogroup, self.h1, self.h2

    @staticmethod
    def get_serovar(df, sg, h1, h2):
        h2_is_missing = '-' in h2

        b_sg = df['Serogroup'].isin(sg)
        b_h1 = df['H1'].isin(h1)
        if h2_is_missing:
            b_h2 = df['can_h2_be_missing']
        else:
            b_h2 = df['H2'].isin(h2)

        df_prediction = df[(b_sg & b_h1 & b_h2)]
        logger.info('Serovar prediction for {}:{}:{} is {}'.format(sg, h1, h2, list(df_prediction['Serovar'])))
        if df_prediction.shape[0] > 0:
            return '|'.join(list(df_prediction['Serovar']))


    def predict_serovar_from_antigen_blast(self):
        if not self.serogroup or not self.h2 or not self.h1:
            self.predict_antigens()

        df = serovar_table()
        sg = self.serogroup
        h1 = self.h1
        h2 = self.h2

        for sg_groups in SEROGROUP_SIMILARITY_GROUPS:
            if sg in sg_groups:
                sg = sg_groups
                break

        if not isinstance(sg, list):
            sg = [sg]

        for h1_groups in H1_FLIC_SIMILARITY_GROUPS:
            if h1 in h1_groups:
                h1 = h1_groups
                break

        if not isinstance(h1, list):
            h1 = [h1]

        for h2_groups in H2_FLJB_SIMILARITY_GROUPS:
            if h2 in h2_groups:
                h2 = h2_groups
                break

        if not isinstance(h2, list):
            h2 = [h2]

        self.serovar = SerovarPredictor.get_serovar(df, sg, h1, h2)

        return self.serovar

    def get_serovar_prediction(self, session, user, genome):
        serovar_pred = SerovarPrediction()
        sg_pred = self.serogroup_predictor.serogroup_prediction
        wzx_pred = self.serogroup_predictor.wzx_prediction
        wzy_pred = self.serogroup_predictor.wzy_prediction
        h1_pred = self.h1_predictor.h1_prediction
        h2_pred = self.h2_predictor.h2_prediction
        sg_pred.user = user
        sg_pred.genome = genome
        wzx_pred.user = user
        wzx_pred.genome = genome
        wzy_pred.user = user
        wzy_pred.genome = genome
        serovar_pred.serogroup_prediction = sg_pred
        serovar_pred.serogroup = self.serogroup
        h1_pred.genome = genome
        h1_pred.user = user
        serovar_pred.h1_flic_prediction = h1_pred
        serovar_pred.h1 = self.h1
        h2_pred.genome = genome
        h2_pred.user = user
        serovar_pred.h2_fljb_prediction = h2_pred
        serovar_pred.h2 = self.h2
        serovar_pred.user = user
        serovar_pred.genome = genome

        return serovar_pred


def mlst_to_serovar_matrix_table():
    df = pd.read_csv(MLST_TO_SEROVAR_MATRIX_PATH, index_col=0)
    return df


def get_mlst_serovar_prediction(genome, serovar_pred):
    """
    Determine the serovar prediction based off the MLST ST if the genome has an MLST ST assigned.

    Notes:
        The MLST ST to serovar table was generated from a supplementary table from
        Achtmann et al 2012 paper

        http://journals.plos.org/plospathogens/article?id=10.1371/journal.ppat.1002776

    Args:
        genome (app.models.Genome): User_ Genome_ object
        serovar_pred (app.models.SerovarPrediction): SerovarPrediction_ object
    """
    mlst_test_id = session.query(MistTest.id).filter(MistTest.name == 'MLST').scalar()
    mlst_mist_result = genome.mist_metadata_results \
        .filter(MistMetadataResult.test_id == mlst_test_id) \
        .first()
    if mlst_mist_result is not None:
        mlst_st = mlst_mist_result.attrs['ST']
        if mlst_st != '':
            df_mlst_to_serovar = mlst_to_serovar_matrix_table()
            if mlst_st not in df_mlst_to_serovar.columns:
                return
            series_mlst_serovars = df_mlst_to_serovar[mlst_st]
            series_mlst_serovars = series_mlst_serovars[~pd.isnull(series_mlst_serovars)]
            mlst_serovar_counts = {i: int(v) for i, v in series_mlst_serovars.iteritems()}
            counter_mlst_serovar_counts = Counter(mlst_serovar_counts)
            top_serovar_by_mlst = counter_mlst_serovar_counts.most_common(1)[0][0]
            serovar_pred.mlst_serovar_counts = mlst_serovar_counts
            serovar_pred.serovar_mlst = top_serovar_by_mlst


def get_sgsa_predicted_antigens(genome, serovar_pred):
    """
    Add the SGSA predicted antigens to the SerovarPrediction object.
    If the SGSA H1, H2 and Serogroup assignments exist, add them to the
    SerovarPrediction object.

    Notes:
        If H0 is not null and H1 is null, then H1 is assigned H0.

        If H0 is not null and H2 is null, then H2 is assigned H0.

    Args:
        genome (app.models.Genome): User_ Genome_ object
        serovar_pred (app.models.SerovarPrediction): SerovarPrediction_ object
    """
    sgsa_results = genome.mist_metadata_results.join(MistTest).filter(MistTest.name == 'SGSA').first()
    if sgsa_results is not None:
        sgsa_results_dict = sgsa_results.attrs
        serovar_pred.serogroup_sgsa = sgsa_results_dict['Serogroup']
        serovar_pred.h1_sgsa = sgsa_results_dict['H1']
        serovar_pred.h2_sgsa = sgsa_results_dict['H2']
        if serovar_pred.h1_sgsa == '' and sgsa_results_dict['H0'] != '':
            serovar_pred.h1_sgsa = sgsa_results_dict['H0']
        elif serovar_pred.h2_sgsa == '' and sgsa_results_dict['H0'] != '':
            serovar_pred.h2_sgsa = sgsa_results_dict['H0']


def determine_serovar_from_cgmlst_and_antigen_blast(public_username, genome, serovar_pred, serovar_predictor, user):
    """
    Determine the serovar from cgMLST cluster membership analysis and antigen BLAST results.
    SerovarPrediction object is assigned H1, H2 and Serogroup from the antigen BLAST results.
    Antigen BLAST results will predict a particular serovar or list of serovars, however,
    the cgMLST membership may be able to help narrow down the list of potential serovars.

    Notes:
        If the cgMLST predicted serovar is within the list of antigen BLAST predicted serovars,
        then the serovar is assigned the cgMLST predicted serovar.


        If all antigens are found, but an antigen serovar is not found then the serovar is assigned
        a pseudo-antigenic formula (Serogroup:H1:H2), otherwise the serovar is assigned the cgMLST prediction.


        If the antigen predicted serovar does not match the cgMLST predicted serovar,

        - the serovar is the cgMLST serovar if the cgMLST cluster level is <= 0.1 (10% or less)
        - otherwise, the serovar is antigen predicted serovar(s)

    Args:
        genome (app.models.Genome): User_ Genome_ object
        serovar_pred (app.models.SerovarPrediction): SerovarPrediction_ object
        serovar_predictor (app.serovar_prediction.SerovarPredictor): SerovarPredictor_ object
        user (app.models.User): User_ object
    """
    genome_clusters = genome.cgmlst_clusters
    df_clusters = public_genomes_clusters_table(public_username)
    cluster_number, matching_cluster_level = get_cluster_level_and_number(df_clusters, genome_clusters)
    h1 = serovar_predictor.h1
    h2 = serovar_predictor.h2
    sg = serovar_predictor.serogroup
    serovar_pred.serovar_antigen = serovar_predictor.serovar
    if matching_cluster_level is None:
        logger.warning('No matching cluster level for genome {} of user {}'.format(genome.name, user.name))
        serovar_pred.serovar = serovar_predictor.serovar
    else:
        genomes_in_cluster = list(df_clusters[df_clusters[matching_cluster_level] == cluster_number].index)
        public_user_id = session.query(User.id).filter(User.name == public_username).scalar()
        name_serovar = {name: serovar for name, serovar in
                        session.query(Genome.name, Genome.serovar)
                        .join(CurationInfo)
                        .filter(Genome.user_id == public_user_id, Genome.is_analyzed, CurationInfo.is_trusted)
                        .all()}
        serovars_in_cluster = [name_serovar[x] for x in genomes_in_cluster]
        counter_serovars_in_cluster = Counter(serovars_in_cluster)
        logger.info(counter_serovars_in_cluster)
        top_serovar_by_cgmlst = None
        try:
            top_serovar_by_cgmlst = counter_serovars_in_cluster.most_common(1)[0][0]
            serovar_pred.serovar_cgmlst = top_serovar_by_cgmlst
            serovar_pred.cgmlst_cluster_number = cluster_number
            serovar_pred.cgmlst_cluster_level = matching_cluster_level
            serovar_pred.cgmlst_serovar_counts = dict(counter_serovars_in_cluster)
        except:
            logger.warning('No cgMLST clusters available for serovar prediction for genome {}'.format(genome))
        if serovar_predictor.serovar is None:
            if (h1 is not None or h1 != '') and (h2 is not None or h2 != '') and (sg is not None or sg != ''):
                # TODO: add subspecies designation into antigenic formula
                serovar_pred.serovar = '{}:{}:{}'.format(sg, h1, h2)
            else:
                serovar_pred.serovar = top_serovar_by_cgmlst
        else:
            serovars_from_antigen = serovar_predictor.serovar.split('|')
            if not isinstance(serovars_from_antigen, list):
                serovars_from_antigen = [serovars_from_antigen]
            if top_serovar_by_cgmlst is not None:
                if top_serovar_by_cgmlst in serovars_from_antigen:
                    serovar_pred.serovar = top_serovar_by_cgmlst
                else:
                    if float(matching_cluster_level) <= 0.1:
                        serovar_pred.serovar = top_serovar_by_cgmlst
                    else:
                        serovar_pred.serovar = serovar_predictor.serovar