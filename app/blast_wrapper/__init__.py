from datetime import datetime
import shutil
from subprocess import Popen, PIPE
from app import logger
import os
import pandas as pd


BLAST_TABLE_COLS = '''
qseqid
sseqid
pident
length
mismatch
gapopen
qstart
qend
sstart
send
evalue
bitscore
qlen
slen
sseq
'''.strip().split('\n')


class BlastRunner:
    blast_db_created = False

    def __init__(self, fasta_path, tmp_work_dir):
        self.tmp_work_dir = tmp_work_dir
        self.fasta_path = fasta_path

    def _create_tmp_folder(self):
        try:
            os.makedirs(self.tmp_work_dir)
        except:
            pass
        return self.tmp_work_dir

    def _copy_fasta_to_work_dir(self):
        filename = os.path.basename(self.fasta_path)
        dest_path = os.path.join(self.tmp_work_dir, filename)
        if self.fasta_path == dest_path:
            self.tmp_fasta_path = dest_path
            return dest_path
        shutil.copyfile(self.fasta_path, dest_path)
        self.tmp_fasta_path = dest_path
        return dest_path

    def _run_makeblastdb(self):
        work_dir = os.path.dirname(self.tmp_fasta_path)
        filename = os.path.basename(self.tmp_fasta_path)
        nin_filepath = os.path.join(work_dir, filename + '.nin')
        if os.path.exists(nin_filepath):
            self.blast_db_created = True
            return self.tmp_fasta_path

        p = Popen(['makeblastdb',
                   '-in', self.tmp_fasta_path,
                   '-dbtype', 'nucl'],
                  stdout=PIPE,
                  stderr=PIPE)
        p.wait()
        stdout = p.stdout.read()
        stderr = p.stderr.read()
        logger.info('makeblastdb on {0} STDOUT: {1}'.format(self.tmp_fasta_path, stdout))
        logger.info('makeblastdb on {0} STDERR: {1}'.format(self.tmp_fasta_path, stderr))

        if os.path.exists(nin_filepath):
            self.blast_db_created = True
            return self.tmp_fasta_path
        else:
            ex_msg = 'makeblastdb was not able to create a BLAST DB for {0}. STDERR: {1}'.format(filename, stderr)
            logger.error(ex_msg)
            raise Exception(ex_msg)

    def blast_against_query(self, query_fasta_path):

        if not self.blast_db_created:
            self.prep_blast()

        gene_filename = os.path.basename(query_fasta_path)
        genome_filename = os.path.basename(self.tmp_fasta_path)
        timestamp = '{:%Y%b%d_%H_%M_%S}'.format(datetime.now())
        outfile = os.path.join(self.tmp_work_dir, '{}-{}-{}.blast'.format(gene_filename,
                                                                          genome_filename,
                                                                          timestamp))
        p = Popen(['blastn',
                   '-query', query_fasta_path,
                   '-db', self.tmp_fasta_path,
                   '-evalue', '1e-6',
                   '-out', outfile,
                   '-outfmt', '6 std qlen slen sseq'],
                  stdout=PIPE,
                  stderr=PIPE)

        p.wait()

        stdout = p.stdout.read()
        stderr = p.stderr.read()
        if stdout:
            logger.info('blastn on db {} and query {} STDOUT: {}'.format(genome_filename, gene_filename, stdout))
        if stderr:
            logger.info('blastn on db {} and query {} STDERR: {}'.format(genome_filename, gene_filename, stderr))

        if os.path.exists(outfile):
            return outfile
        else:
            ex_msg = 'blastn on db {} and query {} did not produce expected output file at {}'.format(genome_filename,
                                                                                                      gene_filename,
                                                                                                      outfile)
            logger.error(ex_msg)
            raise Exception(ex_msg)

    def cleanup(self):
        self.blast_db_created = False
        shutil.rmtree(self.tmp_work_dir)

    def prep_blast(self):
        self._create_tmp_folder()
        self._copy_fasta_to_work_dir()
        self._run_makeblastdb()

    def run_blast(self, query_fasta_path):
        self.prep_blast()
        blast_outfile = self.blast_against_query(query_fasta_path)
        return blast_outfile


class BlastReader:
    is_missing = True
    is_perfect_match = False
    is_trunc = False


    def __init__(self, blast_outfile):
        """
        Read BLASTN output file into a pandas DataFrame.
        Sort the DataFrame by BLAST bitscore.
        If there are no BLASTN results, then no results can be returned.

        @type blast_outfile: str
        @param blast_outfile: BLASTN output file path
        """
        self.blast_outfile = blast_outfile
        try:
            self.df = pd.read_table(self.blast_outfile, header=None)
            self.df.columns = BLAST_TABLE_COLS
            # calculate the coverage for when results need to be validated
            self.df['coverage'] = self.df.length / self.df.qlen
            self.df.sort('bitscore', ascending=False, inplace=True)
            self.is_missing = False
        except Exception as e:
            logger.info('{} -- {}'.format(e.message, blast_outfile))
            self.is_missing = True


    def df_dict(self):
        if not self.is_missing:
            return self.df.to_dict()

    @staticmethod
    def df_first_row_to_dict(df):
        if df is not None:
            return [dict(r) for i, r in df.head(1).iterrows()][0]

    @staticmethod
    def is_blast_result_trunc(qstart, qend, sstart, send, qlen, slen):
        """
        Check if a query sequence is truncated by the end of a subject sequence

        @type qstart: int
        @param qstart: Query sequence start index
        @type qend: int
        @param qend: Query sequence end index
        @type sstart: int
        @param sstart: Subject sequence start index
        @type send: int
        @param send: Subject sequence end index
        @type qlen: int
        @param qlen: Query sequence length
        @type slen: int
        @param slen: Subject sequence length
        @return: True if truncated; False is not truncated
        """
        q_match_len = abs(qstart - qend) + 1
        # s_match_len = abs(sstart - send) + 1
        s_max = max(sstart, send)
        s_min = min(sstart, send)
        if q_match_len < qlen:
            if s_max >= slen or s_min <= 1:
                return True

        return False

    def top_result(self):
        """
        Try to find a 100% identity and coverage result (perfect match).
        If one does not exist, then retrieve the result with the highest bitscore.

        @return: Ordered dict of BLASTN results or None if no BLASTN results generated
        """

        if self.is_missing:
            return None

        df_perfect_matches = self.df[(self.df['coverage'] == 1.0) & (self.df['pident'] == 100.0)]
        if df_perfect_matches.shape[0]:
            self.is_perfect_match = True
            return BlastReader.df_first_row_to_dict(df_perfect_matches)

        # Return the result with the highest bitscore.
        # This is the first result in dataframe since the df is ordered by
        # bitscore in descending order.
        result_dict = BlastReader.df_first_row_to_dict(self.df)
        result_trunc = BlastReader.is_blast_result_trunc(qstart=result_dict['qstart'],
                                                         qend=result_dict['qend'],
                                                         sstart=result_dict['sstart'],
                                                         send=result_dict['send'],
                                                         qlen=result_dict['qlen'],
                                                         slen=result_dict['slen'])
        self.is_trunc = result_trunc
        return result_dict