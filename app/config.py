'''
SISTR backend configuration file

This file contains configuration details for connecting to the Postgres DB,
running MIST, running QUAST and where to store temporary analysis data.
'''
__author__ = 'peter'
import os

#: (str): base directory of SISTR server app
basedir = os.path.abspath(os.path.dirname(__file__))


class MailConfig:
    MAIL_SERVER = os.environ.get('MAIL_SERVER') or 'localhost'
    MAIL_PORT = os.environ.get('MAIL_PORT') or 25
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') or False
    MAIL_USE_SSL = os.environ.get('MAIL_USE_SSL') or False
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or None
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or None
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER') or None
    MAIL_MAX_EMAILS = os.environ.get('MAIL_MAX_EMAILS') or None


class Config(MailConfig):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'some hard to guess string'
    #: (str): path to MIST binary
    MIST_BIN_PATH = os.environ.get('MIST_BIN_PATH') or 'mist_bin/MIST.exe'

    #: (dict): MIST test names to MIST test description file paths
    MIST_MARKERS_FILES = {'MLST': 'mist_assays/MLST.markers',
                          'wgMLST_330': 'mist_assays/wgMLST_330.markers',
                          'rMLST': 'mist_assays/rMLST.markers', }

    #: (str): path to *in silico* MIST assays alleles files
    MIST_ALLELES_DIR = os.environ.get('MIST_ALLELES_DIR') or 'mist_assays/alleles'

    #: (str): public SISTR user name (default=*sistr*)
    PUBLIC_SISTR_USER_NAME = os.environ.get('PUBLIC_SISTR_USER_NAME') or 'sistr'

    #: (str): QUAST script absolute path for generating assembly quality stats
    QUAST_BIN_PATH = os.environ.get('QUAST_BIN_PATH') or '/home/peter/bioinf/quast-2.3/quast.py'

    #: (str): folder to write temporary analysis data to
    TEMP_DIR = os.environ.get('TEMP_DIR') or '/tmp'
    CACHE_TYPE = os.environ.get('CACHE_TYPE') or 'redis'
    CACHE_KEY_PREFIX = os.environ.get('CACHE_KEY_PREFIX') or 'SISTR'


class DevConfig(Config):
    DEBUG = True
    DATABASE_URI = os.environ.get('DEV_DATABASE_URI') or \
                   'postgresql://sistr:sistr_password@localhost:5432/sistr_db'


class TestingConfig(Config):
    TESTING = True
    DATABASE_URI = os.environ.get('TEST_DATABASE_URI') or \
                   'postgresql://sistr:sistr_password@localhost:5432/sistr_test_db'


class ProductionConfig(Config):
    DATABASE_URI = os.environ.get('DEV_DATABASE_URI') or \
                   'postgresql://sistr:sistr_password@localhost:5432/sistr_db'


config = {
    'dev': DevConfig,
    'testing': TestingConfig,
    'prod': ProductionConfig,
    'default': DevConfig,
}
