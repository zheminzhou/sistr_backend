__author__ = 'piotr'

import os
import pytest
from app.tasks import fasta_format_check


TEST_FASTA_PATH = os.path.abspath('test_data/00_0163.fasta')


def test_fasta_format_check():
    user_name = 'tester'

    with open(TEST_FASTA_PATH) as f:
        fasta_str = f.read()

    assert fasta_format_check(fasta_str, user_name=user_name, genome_name='valid_fasta') == fasta_str, \
        'FASTA file contents string not valid FASTA format'

    fasta_str = '''
    AGTCG
    >header 1
    GTAGGC
    '''
    with pytest.raises(Exception):
        fasta_format_check(fasta_str, user_name, genome_name='missing_header')

    fasta_str = '''
    >header 1

    >header 2

    >header 3
    >header 4
    '''

    with pytest.raises(Exception):
        fasta_format_check(fasta_str, user_name, genome_name='only_header_character')

    fasta_str = '''
    >header 1
    AaCcGgTtRrYySsWwKkMmBbDdHhVvNn
    '''

    assert fasta_format_check(fasta_str, user_name, genome_name='valid_nucleotides'), \
        'FASTA file contents string contains invalid nucleotide characters'

    fasta_str = '''
    >header 1
    AGTCQ
    '''

    with pytest.raises(Exception):
        fasta_format_check(fasta_str, user_name, genome_name='invalid_nucleotide_character')

    fasta_str = '''
    >header 1
    12345
    '''

    with pytest.raises(Exception):
        fasta_format_check(fasta_str, user_name, genome_name='invalid_fasta_seq_with_numbers')


# TODO: write tests for checking that genome is added to DB
def test_add_genome_to_db():
    pass

    # TODO: write tests for running QUAST and MIST