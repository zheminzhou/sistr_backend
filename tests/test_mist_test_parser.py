__author__ = 'peter'
from app.models import Genome, MistMarker, MistTest, MistMarkerResult, User, MistMetadataResult

import os

from app.parsers import MistJSONParser
from app.config import PUBLIC_SISTR_USER_NAME


def test_MistJSONParser_wgMLST_load(session):
    u = User(name=PUBLIC_SISTR_USER_NAME)
    session.add(u)
    session.commit()

    mist_json_path = os.path.join('test_data/mist-full.json')
    assert os.path.exists(mist_json_path)

    parser = MistJSONParser(session, u, mist_json_path)
    assert session.query(Genome).first() is not None

    parser.parse_mist_tests()
    assert session.query(MistMarker).first() is not None
    marker = session.query(MistMarker).first()
    assert marker.alleles.count() > 0
    # print(marker.alleles.count())
    assert session.query(MistTest).first() is not None
    assert session.query(MistTest).count() == 1
    test = session.query(MistTest).first()
    assert session.query(MistMarker).count() == test.markers.count()
    parser.parse_all_marker_results()
    assert session.query(MistMarkerResult).filter(MistMarkerResult.test == test).count() == test.markers.count()


def test_MistJSONParser_MLST_load(session):
    u = session.query(User).filter(User.name == PUBLIC_SISTR_USER_NAME).first()

    mist_json_path = os.path.join('test_data', 'mist-mlst-full.json')
    assert os.path.exists(mist_json_path)

    parser = MistJSONParser(session, user=u, json_path=mist_json_path)
    assert session.query(Genome).first() is not None

    parser.parse_mist_tests()
    mlst_test = session.query(MistTest).filter(MistTest.name == 'MLST').first()
    assert mlst_test is not None
    n_mlst_markers = session.query(MistMarker) \
        .join(MistMarker.test) \
        .filter(MistTest.name == 'MLST') \
        .count()

    assert n_mlst_markers == 7
    parser.parse_all_marker_results()
    # should only be 7 MLST results since only 1 genome's worth of results will be loaded
    assert session.query(MistMarkerResult).filter(MistMarkerResult.test == mlst_test).count() == n_mlst_markers
    parser.parse_all_test_metadata()
    mist_metadata_query = session.query(MistMetadataResult).filter(MistMetadataResult.test == mlst_test)
    assert mist_metadata_query.count() > 0
    mist_metadata = mist_metadata_query.first()
    assert mist_metadata is not None
    assert mist_metadata.attrs is not None
    print mist_metadata


def test_MistJSONParser_SGSA_load(session):
    u = session.query(User).filter(User.name == PUBLIC_SISTR_USER_NAME).first()
    mist_json_path = os.path.join('test_data', 'mist-sero-full.json')
    assert os.path.exists(mist_json_path)

    parser = MistJSONParser(session, user=u, json_path=mist_json_path)
    assert session.query(Genome).first() is not None

    parser.parse_mist_tests()
    sero_test = session.query(MistTest).filter(MistTest.name == 'sero').first()
    assert sero_test is not None
    n_sero_markers = session.query(MistMarker) \
        .join(MistMarker.test) \
        .filter(MistTest.name == 'sero') \
        .count()

    assert n_sero_markers == 122
    parser.parse_all_marker_results()
    # should only be 7 MLST results since only 1 genome's worth of results will be loaded
    assert session.query(MistMarkerResult).filter(MistMarkerResult.test == sero_test).count() == n_sero_markers
    parser.parse_all_test_metadata()
    mist_metadata_query = session.query(MistMetadataResult).filter(MistMetadataResult.test == sero_test)
    assert mist_metadata_query.count() > 0
    mist_metadata = mist_metadata_query.first()
    assert mist_metadata is not None
    assert mist_metadata.attrs is not None
    print mist_metadata