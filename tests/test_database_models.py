from __future__ import print_function
from app.models import Genome, MistMarker, MistTest, MistMarkerResult, User
from app.models import MistTestType

from sqlalchemy.exc import IntegrityError, StatementError

import pytest


def test_mist_test(session):
    test1 = MistTest(name='test1', type=MistTestType.allelic)
    session.add(test1)
    session.commit()

    assert session.query(MistTest).first() is not None
    assert session.query(MistTest).count() == 1

    test2 = MistTest(name='test2', type=MistTestType.pcr)

    session.add(test2)
    session.commit()
    assert session.query(MistTest).count() == 2

    assert session.query(MistTest).filter_by(name='notindb').first() is None
    assert session.query(MistTest).filter_by(name='test1').first() is not None


def test_mist_test_query(session):
    t1 = session.query(MistTest).first()
    assert t1 is not None
    assert session.query(MistTest).count() == 2


def test_add_duplicate_mist_test(session):
    assert session.query(MistTest).count() == 2
    test1_dup = MistTest(name='test1', type=MistTestType.pcr)
    with pytest.raises(IntegrityError):
        session.add(test1_dup)
        session.commit()
    session.rollback()
    assert session.query(MistTest).first() is not None


def test_try_add_madeup_test_type(session):
    with pytest.raises(StatementError):
        t = MistTest(name='test2', type='madeuptest')
        session.add(t)
        session.commit()
    session.rollback()


def test_mist_marker_result(session):
    t1 = session.query(MistTest).first()
    assert t1 is not None

    m1 = MistMarker(name='marker1', test=t1)
    session.add(m1)
    session.commit()
    assert t1.markers.count() == 1
    assert t1.markers.first() == m1

    u1 = User(name='user1')
    session.add(u1)
    session.commit()
    g1 = Genome(name='genome1', user=u1)
    mr1 = MistMarkerResult(result='1', marker=m1, test=t1, genome=g1, user=u1)
    session.add(mr1)
    session.commit()

    assert g1.mist_marker_results.count() == 1
    assert g1.mist_marker_results.first() == mr1
    assert g1.id is not None

    # duplicate marker results are not allowed
    with pytest.raises(IntegrityError):
        mr1_dup = MistMarkerResult(result='1', marker=m1, test=t1, genome=g1, user=u1)
        session.add(mr1_dup)
        session.commit()
    session.rollback()

    m2 = MistMarker(name='marker2', test=t1)
    session.add(m2)
    session.commit()

    mr2 = MistMarkerResult(result='2', marker=m2, test=t1, genome=g1, user=u1)
    session.add(mr2)
    session.commit()

    assert g1.mist_marker_results.count() == 2
    assert session.query(MistMarkerResult).filter_by(marker=m2).first() == mr2

    with pytest.raises(IntegrityError):
        mr1 = MistMarkerResult(result='1', marker=m1, test=t1, genome=None, user=None)
        session.add(mr1)
        session.commit()
    session.rollback()


def test_make_unique_genome_name(session):
    existing_genome_name = 'genome1'
    g_name = Genome.make_unique_name(existing_genome_name, session=session)

    assert existing_genome_name != g_name


def test_mist_test_type():
    assert MistTestType.from_string('Allelic') == MistTestType.allelic
    assert MistTestType.from_string('PCR') == MistTestType.pcr
    assert MistTestType.from_string('OligoProbe') == MistTestType.oligo_probe
    assert MistTestType.from_string('SNP') == MistTestType.snp
    assert MistTestType.from_string('AmpliconProbe') == MistTestType.amplicon_probe
