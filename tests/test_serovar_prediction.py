'''
Tests for serovar prediction

The serovar of an uploaded genome is determined by:
- comparison of wgMLST_330 results of uploaded genome to public genomes for
  which the serovar designation is known and trusted
- inference of serovar by looking at the antigen results of the SGSA and BLAST
  results of the wzx, wzy, fliC and fljB genes

If the wgMLST_330 similarity of the uploaded genome to a public genome is
>= 90% then the serovar of the uploaded genome is determined to be the same
as the uploaded genome.

The subspecies of uploaded genomes is determined at the 10% similarity level
for wgMLST_330.
This info can help with determination of the serovar since the subspecies
designation is important for making many serovar calls.


'''
from app.models import User, Genome

__author__ = 'piotr'
from app.blast_wrapper import BlastRunner, BlastReader, BLAST_TABLE_COLS
from app.serovar_prediction import \
    get_antigen_name, \
    WZX_FASTA_PATH, \
    SerovarPredictor
import os
import pandas as pd


def test_get_antigen_name():
    assert get_antigen_name('44|gb|O58|58') == '58'
    assert get_antigen_name('gi|206707319|D1') == 'D1'


def test_BlastRunner():
    fasta_path = 'test_data/00_0163.fasta'
    tmp_dir = 'tmp/test-blast-runner/'
    br = BlastRunner(fasta_path, tmp_dir)
    br.prep_blast()
    assert os.path.exists(tmp_dir)
    fasta_copy_path = os.path.join(tmp_dir, os.path.basename(fasta_path))
    nin_path = fasta_copy_path + '.nin'
    assert os.path.exists(fasta_copy_path)
    assert os.path.exists(nin_path)

    blast_outfile = br.run_blast(WZX_FASTA_PATH)
    assert os.path.exists(blast_outfile)
    # Pandas should be able to parse in the table
    df = pd.read_table(blast_outfile)
    # the number of columns needs to be the expected number based on blastn outfmt specs
    assert len(df.columns) == len(BLAST_TABLE_COLS)

    br.cleanup()
    assert not os.path.exists(tmp_dir)


def test_BlastReader():
    fasta_path = 'test_data/01_O_05.fasta'
    tmp_dir = 'tmp/test-blast-reader/'
    br = BlastRunner(fasta_path, tmp_dir)
    br.prep_blast()
    blast_outfile = br.run_blast(WZX_FASTA_PATH)
    blast_reader = BlastReader(blast_outfile)
    top_result = blast_reader.top_result()
    assert get_antigen_name(top_result['qseqid']) == 'B'
    assert blast_reader.is_perfect_match == True

    # not truncated; match found in the middle of the subject sequence
    assert not BlastReader.is_blast_result_trunc(qstart=1,
                                                 qend=100,
                                                 sstart=101,
                                                 send=200,
                                                 qlen=100,
                                                 slen=1000)

    # not truncated; shorter match (-10bp) found in the middle of the subject
    # sequence
    assert not BlastReader.is_blast_result_trunc(qstart=1,
                                                 qend=90,
                                                 sstart=101,
                                                 send=190,
                                                 qlen=100,
                                                 slen=1000)

    # not truncated; shorter match (-20bp) found in the middle of the subject
    # sequence
    assert not BlastReader.is_blast_result_trunc(qstart=1,
                                                 qend=80,
                                                 sstart=101,
                                                 send=180,
                                                 qlen=100,
                                                 slen=1000)


    # truncated at the start of the subject
    assert BlastReader.is_blast_result_trunc(qstart=51,
                                             qend=100,
                                             sstart=1,
                                             send=50,
                                             qlen=100,
                                             slen=1000)

    # truncated at the end of the subject
    assert BlastReader.is_blast_result_trunc(qstart=51,
                                             qend=100,
                                             sstart=951,
                                             send=1000,
                                             qlen=100,
                                             slen=1000)


def test_SerovarPredictor(session):
    """
    Test SerovarPredictor
    @param session: DB session
    """
    fasta_path = 'test_data/00_0163.fasta'
    tmp_dir = 'tmp/test-blast-runner/'
    br = BlastRunner(fasta_path, tmp_dir)
    sp = SerovarPredictor(br)
    sp.predict_serovar_from_antigen_blast()
    print sp.serogroup, sp.h1, sp.h2, sp.serovar
    sg_pred = sp.serogroup_predictor.serogroup_prediction
    # print sg_pred.__dict__
    wzx_pred = sp.serogroup_predictor.wzx_prediction
    # print wzx_pred.top_result
    wzy_pred = sp.serogroup_predictor.wzy_prediction
    # print wzy_pred.top_result
    h1_pred = sp.h1_predictor.h1_prediction
    # print h1_pred.top_result
    h2_pred = sp.h2_predictor.h2_prediction
    # print h2_pred.top_result
    u = User(name='tester')
    session.add(u)
    g = Genome(name='genome1')
    g.user = u
    session.add(g)
    session.commit()
    u = session.query(User).first()
    g = session.query(Genome).first()
    serovar_pred = sp.get_serovar_prediction(session=session, user=u, genome=g)
    session.add(serovar_pred)
    session.commit()

    assert g.serovar_prediction.first() == serovar_pred
    assert g.serogroup_prediction.first() == sg_pred
    assert g.h1_flic_prediction.first() == h1_pred
    assert g.h2_fljb_prediction.first() == h2_pred
    assert g.wzx_prediction.first() == wzx_pred
    assert g.wzy_prediction.first() == wzy_pred




