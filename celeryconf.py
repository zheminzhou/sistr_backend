'''
Celery task queue configuration file
------------------------------------

SISTR users will only be able to queue tasks via the the REST API.

celerybeat will be used to queue scheduled tasks such as:
- removal of users and genomes from the DB after temporary user expiration
- writing new cgMLST/MLST allele fasta files to disk for use by MIST
'''

# trying to use redis instead of amqp
BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

# respawn celeryd instance after 100 tasks to reduce memory leaks
CELERYD_MAX_TASKS_PER_CHILD=100
# tasks acknowledged as done after finishing and restarted if worker crashes
# http://docs.celeryproject.org/en/latest/configuration.html#celery-acks-late
CELERY_ACKS_LATE=True
# workers can only queue up to 1 unacknowledged task
CELERYD_PREFETCH_MULTIPLIER=1
CELERY_TASK_RESULT_EXPIRES=3600 #expire results in 1 hr

CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_TIMEZONE = 'America/Winnipeg'
CELERY_ENABLE_UTC = True
CELERYD_FORCE_EXECV = True

CELERY_ROUTES = {'app.tasks.send_email': {'queue': 'email'}}

from celery.schedules import crontab

CELERYBEAT_SCHEDULE = {
    'sistr-write_mist_alleles': {
        'task': 'app.tasks.write_mist_alleles',
        'schedule': crontab(minute=0, hour=0),
        'args': []
    },
    'sistr-delete_old_tmp_users': {
        'task': 'app.tasks.delete_old_tmp_users',
        'schedule': crontab(minute=0, hour=4),
        'args': []
    },
}